<?php
$selfversion="1.05";


if (file_exists("schulbox2.php")) {
	include_once("schulbox2.php");
}

$config=array("datapath" => "/home/user-data/schule","slug_include_city" => true, "maildomain" => "x.xizz.de","mailhost" => "testbox.x.xizz.de", 
		"admin_id" => "me","slug_prefix" => "", "slug_appendix" => "","mib_admin_username" => "me@x.xizz.de","mib_admin_password" => "yVuD@4ej_a-i",
		"debuglevel" => 5);
//"slug_prefix" => "pfx.", "slug_appendix" => "_apx",


/* =============== todo

storage: confirmtokens nach 24h löschen
storage: signup:x:* 60 Tage nach Lastupdate löschen
storage: passwordtokens nach 24h löschen
ou: invitecode löschen, wenn E-Mail bestätigt
passwordtoken: alte Tokens löschen, wenn neues generiert wird


regulär: (root) crontab prüfen, ggf entragen

---- erledigt

=========================== */





// --------------------------
$config["database"]=$config["datapath"]."/schule.db";
$config["databaseLOG"]=$config["datapath"]."/schulelog.db";

// run as command line script
if (runfromcli()) {
	ini_set('register_argc_argv', true);
	if ($argc<=1) {
		commandline_help();
		die();
	}
	// general checks
	echo "* checking data path ".$config["datapath"]."\n";
	if (file_exists($config["datapath"])) {
		if (is_dir($config["datapath"])) {
			echo "- OK\n";
		} else {
			errormessage("! datapath ".$config["datapath"]." is present but a file");
			die();
		}
	} else {
		mkdir($config["datapath"]);
		chown($config["datapath"],"www-data");
		chgrp($config["datapath"],"www-data");
		echo "- created\n";
	}
	echo "* checking log database ".$config["databaseLOG"]."\n";
	if (!file_exists($config["databaseLOG"])) {
		echo "- creating log database\n";
		$dbLOG = new SQLite3($config["databaseLOG"]);
		chown($config["databaseLOG"],"www-data");
		chgrp($config["databaseLOG"],"www-data");
		$cmd=db_table_schema("sx_log");
		$res=db_exec($dbLOG,$cmd);
		if (!$res) { errormessage("unable to create table sx_log",$dbLOG->lastErrorMsg()); die(); }
	}
	echo "* checking database ".$config["database"]."\n";
	if (file_exists($config["database"])) {
		echo "- exists\n";
		$db = new SQLite3($config["database"]);
	} else {
		$db = new SQLite3($config["database"]);
		chown($config["database"],"www-data");
		chgrp($config["database"],"www-data");
		echo "- created\n";
	}
	echo "* check database version\n";
		db_update_schema($db);



		
		
	if ($argv[1]=="init") {

		//db_add_ou($db,array("name" => "8. Städtisches Gymnasium ","address_city" => "Knotenburg-Dettingen".chr(9)."(Erms)/Teck","address_street" => "AHauptstrasse 7", "address_postcode" => "77777","email" => "poststelle@schule", "personal_email" => "t-boehm+zsg@posteo.de","personal_name" => "Al Taname", "abbreviation" => ""));
		//db_add_ou($db,array("name" => "8. Städtisches Gymnasium ","address_city" => "Knotenburg-Dettingen".chr(9)."(Erms)/Teck","address_street" => "AHauptstrasse 7", "address_postcode" => "77777","email" => "poststelle@schule", "personal_email" => "t-boehm+zsg@posteo.de","personal_name" => "Al Taname", "abbreviation" => ""));
		
$csvstuff=<<<EOM
EOM;

$oulist=explode("\n",$csvstuff);
foreach($oulist as $ouid => $oucsvline) {
	$oucsvdata=explode(chr(9),$oucsvline);
	$lkr=$oucsvdata[11];
	$lkr=str_replace("(LKR)","",$lkr);
	$lkr=str_replace("(SKR)","",$lkr);
	$lkr=trim($lkr);
	$cat_traeg=str_replace("-","",$oucsvdata[14]);
	$cat_traeg=str_replace("<kein Eintrag>","",$cat_traeg);
	$ci_schulgr=str_replace("-","",$oucsvdata[17]);
	$ci_klassen=str_replace("-","",$oucsvdata[18]);
	$ci_lehrkr=str_replace("-","",$oucsvdata[19]);
	$categories=array("landkreis" => $lkr,"schultraeger" => $cat_traeg);
	$custominfo=array("schulgroesse" => $ci_schulgr,"lehrkraefte" =>$ci_lehrkr,"klassen" =>$ci_klassen);
	$ou=array("name" => $oucsvdata[0], "address_city" => $oucsvdata[4],"address_street" => $oucsvdata[2],"address_postcode" => $oucsvdata[3],"email" => $oucsvdata[7].".invalid", 
						"personal_email" => "" ,"personal_name" => "","abbreviation" => "","external_id" => $oucsvdata[1],"categories" => $categories,"custominfo" => $custominfo);
	db_add_ou($db,$ou);
}

		
		
	}
	if ($argv[1]=="cron") {
		xlog("startup cron","*");
		$db=db_getlink("self");
		$dbNC=db_getlink("NC");
		$done=0;
		// check root tasks
		debuglog("checking root tasks",2);
		$where="where storage_key LIKE 'roottasks:%' and storage_value!='' order by \"storage_key\";";
		$roottasks=db_get_multi_entryC($db,"sl_storage",$where);
		debuglog("  Q $where\n  A[roottasks]".anytext($roottasks),9);
		foreach($roottasks as $rtdbid => $rtdata) {
			$done++;
			debuglog("- processing ".$rtdata["storage_key"],3);
			if (isset($rtdata["storage_value"])) {
				$tasklist=unserialize($rtdata["storage_value"]);
				foreach($tasklist as $taskid => $taskinfo) {
					debuglog("  roottask ".$taskid.": ".$taskinfo["task"],4);
					if ($taskinfo["task"]=="mail_template") {
						debuglog("    ".anytext($taskinfo),6);
						mail_template($taskinfo["parameters"]["template"],$taskinfo["parameters"]["params"]);
					}
					if ($taskinfo["task"]=="change_forwarding") {
						debuglog("    ".anytext($taskinfo),6);
						change_forwarding($taskinfo["parameters"]["ou_id"],$taskinfo["parameters"]["newforward"]);
					}
				}
			}
			$sql="delete from sl_storage where storage_key='".SQLite3::escapeString($rtdata["storage_key"])."';";
			$dbx=db_exec($db,$sql);
			
		}
		// check signup form todos
		debuglog("checking signup form submissions",2);
		$formid=db_get_config($db,"form_signup:id");
		debuglog("form ID=".anytext($formid),3);
		if ($formid===null) {
			errormessage("! cannot check signup forms, ID not configured");
			$done++;
		} else {
			$fslastcheck=db_get_single_entry($db,"storage","internal:form_signup_lastcheck");
			$sql="select MAX(timestamp) from oc_forms_v2_submissions;";
			$dbx=db_query($dbNC,$sql);
			$maxts=0;
			if (isset($dbx[0][0])) { $maxts=$dbx[0][0]*1; }
			debuglog("signup lastcheck:".anytext($fslastcheck)." lastsub:".anytext($maxts),4);
			if (($fslastcheck===null) || ($fslastcheck < $maxts)) {
				$tsw="";
				if ($fslastcheck!==null) { $tsw=" and timestamp > ".$fslastcheck; }
				$where="where form_id=".($formid*1).$tsw;
				$subs=db_get_multi_entryC($dbNC,"oc_forms_v2_submissions",$where);
				debuglog("signup subs:".anytext($subs),9);
				if (!(($subs===null) or (count($subs)==0))) {
					foreach($subs as $subr => $subdata) {
						$done++;
						if ($subdata["timestamp"] > $maxts) { $maxts=$subdata["timestamp"]; }
						
						$subid=$subdata["id"];
						debuglog("processing submission #".$subid,5);
						$where="where submission_id=".($subid*1);
						$subanswers=db_get_multi_entryC($dbNC,"oc_forms_v2_answers",$where);
						$subdetails=array();
						foreach($subanswers as $said => $sadata) {
							$where="where config_key LIKE 'form_signup:field_%' and config_value=".($sadata["question_id"]*1);
							$fieldid=db_get_multi_entryC($db,"sl_config",$where);
							if (!isset($fieldid[0]["config_key"])) {
								errormessage("! field id ".($sadata["question_id"]*1)." from submission $subid not found");
								die();
							}
							$detailsname=str_replace("form_signup:field_","",$fieldid[0]["config_key"]);
							debuglog("  Q $where\n  A[fieldid]".anytext($fieldid),9);
							$subdetails[$detailsname]=$sadata["text"];
						}
						if (isset($subdetails["invitecode_id"])) {
							$ic=invitecode_clean($subdetails["invitecode_id"]);
							//$ic="d555a7077bd2";
							$where="where ou_invitecode='".SQLite3::escapeString($ic)."';";
							$icresult=db_get_single_entryC($db,"sl_orgunit",$where);
							debuglog("  Q $where\n  A[icresult]".anytext($icresult),9);
							if ($icresult===null) {
								errormessage("! unable to process submission $subid; invalid invite code");
								mail_template("confirm_invalidcode",array("to" => $subdetails["useremail_id"]));
								db_set_single_entry($db,"storage","signup:".$subid.":status","invalid_code");
							} else {
								$confirmtoken=random_str(64);
								//$confirmdetails=array("submissionid" => $subid,"ou_id" => $icresult["ou_id"], "ou_slug" => $icresult["ou_slug"], "userdata" => $subdetails);
								$oudetails=array("ou_id" => $icresult["ou_id"], "ou_slug" => $icresult["ou_slug"],"ou_invitecode" => $icresult["ou_invitecode"]);
								$confirmdetails=array("submissionid" => $subid,"created" => time());
								db_set_single_entry($db,"storage","confirmtoken:".$confirmtoken,serialize($confirmdetails));
								$subdetails["confirmtoken"]=$confirmtoken;
								$mailvars=array("confirmtoken" =>  $confirmtoken,"confirmurl" => "https://".getconfig("mailhost")."/cloud/schulbox.php?confirm=".$confirmtoken);
								mail_template("confirm",array("to" => $subdetails["useremail_id"],"vars" => $mailvars));
								db_set_single_entry($db,"storage","signup:".$subid.":status","wait_confirm");
								db_set_single_entry($db,"storage","signup:".$subid.":oudata",serialize($oudetails));
								debuglog("successfully processed submission #".$subid,3);
							}
						} else {
							errormessage("! unable to process submission $subid; no invite code");
							db_set_single_entry($db,"storage","signup:".$subid.":status","no_code");
						}
						db_set_single_entry($db,"storage","signup:".$subid.":lastupdate",time());
						db_set_single_entry($db,"storage","signup:".$subid.":userdata",serialize($subdetails));
						db_set_single_entry($db,"storage","internal:form_signup_lastcheck",$maxts);
						
						
						debuglog("signup sub details $subid:".anytext($subdetails),4);
						//db_set_single_entry($db,"storage","signup:".$subid.":status","mailsent");
						//db_set_single_entry($db,"storage","signup:".$subid.":lastupdate",time());
					}
				} else {
					debuglog("no submissions pending",3);
					
				}
			}
		}
		xlog("finish cron:".$done,"*");
		
	}
	//echo "argv:".print_r($argv,true);
} else {
	if (!db_checkdbs()) {
		webif_single("#:html:Error#","Database(s) not set up, please check with Admin");
		die();
	}
	$db=db_getlink("self");
	$dbNC=db_getlink("NC");
	// signup confirmation page
	if (getvar("confirm")!==null) {
		$confstrg=db_get_single_entry($db,"storage","confirmtoken:".getvar("confirm"));
		if ($confstrg===null) {
			webif_single("#:html:Error#","#:html:Invalid link#"); 
			errormessage("signup_confirmation: invalid link:".getvar("confirm"));
			die();
		}
		$confdata=unserialize($confstrg);
		debuglog("signup_confirmation: confdata:".anytext($confdata),8);
		if (!isset($confdata["submissionid"])) {
			webif_single("#:html:Error#","#:html:Internal Error#: subid not present"); 
			errormessage("signup_confirmation: error: submission id not present; link:".getvar("confirm"));
			die();
		}
		$subid=$confdata["submissionid"];
		$substatus=db_get_single_entry($db,"storage","signup:".$subid.":status");
		debuglog("signup_confirmation: substatus:".anytext($substatus),8);
		if ($substatus!=="wait_confirm") {
			webif_single("#:html:Error#","#:html:Email address already confirmed#");
			errormessage("signup_confirmation: error: email address already confirmed; link:".getvar("confirm"));
			die();
		}
		$suboudata=db_get_single_entry($db,"storage","signup:".$subid.":oudata");
		if ($suboudata!==null) { $suboudata=unserialize($suboudata); }
		$subuserdata=db_get_single_entry($db,"storage","signup:".$subid.":userdata");
		if ($subuserdata!==null) { $subuserdata=unserialize($subuserdata); }

		$where="where ou_id=".($suboudata["ou_id"]*1).";";
		$ouresult=db_get_single_entryC($db,"sl_orgunit",$where);
		if ($ouresult===null) {
			webif_single("#:html:Error#","#:html:Internal Error#: ou_id not present"); 
			errormessage("signup_confirmation: error: ou_id not present:".($suboudata["ou_id"]*1)."; link:".getvar("confirm"));
			die();
		}
		debuglog("signup_confirmation: ouresult:".anytext($ouresult),8);
		$mainaddress=$ouresult["ou_slug"]."@".getconfig("maildomain");
		debuglog($mainaddress.": confirmed new email address",3);
		$mailvars=array("passwordlink" => "[passwordlink]","mainaddress" => $mainaddress,"aliasaddresses" => $ouresult["ou_mib_email_aliases"],"personaladdress" =>  $subuserdata["useremail_id"]);
		$mailvars["personname"]=$ouresult["ou_personal_name"];
		// === notify old user
		if ($ouresult["ou_mib_email"]!="") { mail_template("handover",array("to" => $ouresult["ou_personal_email"],"vars" => $mailvars)); }
		debuglog($mainaddress.": previous user notified",5);
		// === change account password
		$randompassword=random_str(64);
		mib_sendcommand("/admin/mail/users/password","POST",array("email" => $ouresult["ou_mib_email"],"password" => $randompassword));
		debuglog($mainaddress.": password invalidated",5);
		// === create passwordtoken
		$passwordtoken=random_str(64);
		$passworddetails=array("userid" => $mainaddress,"created" => time());
		db_set_single_entry($db,"storage","passwordtoken:".$passwordtoken,serialize($passworddetails));
		debuglog($mainaddress.": token for password change created",5);
		$mailvars["passwordreseturl"] = "https://".getconfig("mailhost")."/cloud/schulbox.php?pwreset=".$passwordtoken;
		// === set new user data in ou + remove invite code
		$setabbrev=""; $usealias="";
		//     set abbreviation only if not already set + update alias
		//debuglog("abbrev_zefix \nsubuserdata:".anytext($subuserdata)."\nouresult:".anytext($ouresult),1);
		if ($subuserdata["abbreviation_id"]!="") {
			if ($ouresult["ou_abbreviation"]=="") {
				$setabbrev=", ou_abbreviation='".SQLite3::escapeString(strtoupper($subuserdata["abbreviation_id"]))."'";
				$tmpslug=substr(getslugpart($subuserdata["abbreviation_id"]),0,db_get_config($db,"general:use_abbreviationfield"));
				if (getconfig("slug_include_city")===true) {
					$tmpslug.=".".getslugpart($ouresult["ou_address_city"],"_",false);
				}
				$usealias=$tmpslug;
				$ouresult["ou_abbreviation"]=strtoupper($subuserdata["abbreviation_id"]);
				debuglog($mainaddress.": setting mail alias to".$ouresult["ou_abbreviation"],2);
				$rc=mib_sendcommand("/admin/mail/aliases/add","POST",array("update_if_exists" => 0,"address" => $tmpslug."@".getconfig("maildomain"), "forwards_to" => $mainaddress,"permitted_senders" => ""));
				if ($rc["headers"]["http_code"]!=="200") {
					$usealias="";
				}
				
			} else { 
				debuglog($mainaddress.": ignoring abbreviation; already set",1);
			}
		}
		if ($usealias!="") {
			$mailvars["aliasaddresses"].=" ".$usealias;
		}
		$mailvars["aliasaddresses"]=str_replace(" ","; ",trim($mailvars["aliasaddresses"]));
		if ($mailvars["aliasaddresses"]=="") { $mailvars["aliasaddresses"]="-"; }
		$cmd="update sl_orgunit set ou_personal_name='".SQLite3::escapeString($subuserdata["username_id"])."'".
															 ", ou_personal_email='".SQLite3::escapeString($subuserdata["useremail_id"])."'".
															 ", ou_personal_phone='".SQLite3::escapeString($subuserdata["userphone_id"])."'".
															 ", ou_invitecode=''".
															 $setabbrev.
															 " where ou_id=".($suboudata["ou_id"]*1).";";
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage($mainaddress.": unable to update user in DB ($cmd) ",$db->lastErrorMsg()); die(); }
		$roottasks=array();
		// === (root) change forwarding
		$roottasks[]=array("task" => "change_forwarding","parameters" => array("ou_id" => ($suboudata["ou_id"]*1), "newforward" => $subuserdata["useremail_id"]));
		$mailvars["personname"]=$subuserdata["username_id"];
		// === (root) notify new user (personal address)
		$roottasks[]=array("task" => "mail_template","parameters" => array("template" => "success", "params" => array("to" => $subuserdata["useremail_id"],"vars" => $mailvars)));
		// === (root) notify new user (new address)
		$roottasks[]=array("task" => "mail_template","parameters" => array("template" => "success", "params" => array("to" => $mailvars["mainaddress"],"vars" => $mailvars)));
		$rtid=time().".".random_str(16);
		
		// reset user
		db_reset_nc_user($mainaddress);

		db_set_single_entry($db,"storage","roottasks:".$rtid,serialize($roottasks)); 
		debuglog($mainaddress.": root tasks prepared with ID ".$rtid,5);
		db_set_single_entry($db,"storage","signup:".$subid.":status","confirmed"); 
		db_set_single_entry($db,"storage","signup:".$subid.":lastupdate",time());
		webif_single("#:html:_success_web#","#:html:_success_signup_info#"); 
		die();
	}
	
		if (getvar("pwreset")!==null) {
		$confstrg=db_get_single_entry($db,"storage","passwordtoken:".getvar("pwreset"));
		if ($confstrg===null) {
			webif_single("#:html:Error#","#:html:Invalid link#"); 
			errormessage("password_reset: invalid link:".getvar("pwreset"));
			die();
		}
		$confdata=unserialize($confstrg);
		debuglog("password_reset: confdata:".anytext($confdata),8);
		if (isset($confdata["used"])) {
			webif_single("#:html:Error#","#:html:Link already used#"); 
			errormessage("password_reset: error: link already used; link:".getvar("pwreset"));
			die();
		}
		if (!isset($confdata["userid"])) {
			webif_single("#:html:Error#","#:html:Internal Error#: userid not present"); 
			errormessage("password_reset: error: user id not present; link:".getvar("pwreset"));
			die();
		}
		$userid=$confdata["userid"];
		webif_pwreset(getvar("pwreset"));
		die();
		$substatus=db_get_single_entry($db,"storage","signup:".$subid.":status");
		debuglog("signup_confirmation: substatus:".anytext($substatus),8);
		if ($substatus!=="wait_confirm") {
			webif_single("#:html:Error#","#:html:Email address already confirmed#");
			errormessage("signup_confirmation: error: email address already confirmed; link:".getvar("confirm"));
			die();
		}
		$suboudata=db_get_single_entry($db,"storage","signup:".$subid.":oudata");
		if ($suboudata!==null) { $suboudata=unserialize($suboudata); }
		$subuserdata=db_get_single_entry($db,"storage","signup:".$subid.":userdata");
		if ($subuserdata!==null) { $subuserdata=unserialize($subuserdata); }

		$where="where ou_id=".($suboudata["ou_id"]*1).";";
		$ouresult=db_get_single_entryC($db,"sl_orgunit",$where);
		if ($ouresult===null) {
			webif_single("#:html:Error#","#:html:Internal Error#: ou_id not present"); 
			errormessage("signup_confirmation: error: ou_id not present:".($suboudata["ou_id"]*1)."; link:".getvar("confirm"));
			die();
		}
		debuglog("signup_confirmation: ouresult:".anytext($ouresult),8);
		$mainaddress=$ouresult["ou_slug"]."@".getconfig("maildomain");
		debuglog($mainaddress.": confirmed new email address",3);
		$mailvars=array("passwordlink" => "[passwordlink]","mainaddress" => $mainaddress,"aliasaddresses" => $ouresult["ou_mib_email_aliases"],"personaladdress" =>  $subuserdata["useremail_id"]);
		$mailvars["personname"]=$ouresult["ou_personal_name"];
		// === notify old user
		if ($ouresult["ou_mib_email"]!="") { mail_template("handover",array("to" => $ouresult["ou_personal_email"],"vars" => $mailvars)); }
		debuglog($mainaddress.": previous user notified",5);
		// === change account password
		$randompassword=random_str(64);
		mib_sendcommand("/admin/mail/users/password","POST",array("email" => $ouresult["ou_mib_email"],"password" => $randompassword));
		debuglog($mainaddress.": password invalidated",5);
		// === create passwordtoken
		$mailvars["passwordreseturl"] = generate_passwordresetlink($db,$mainaddress);
		// === set new user data in ou + remove invite code
		$setabbrev=""; 
		//     set abbreviation only if not already set + update alias
		debuglog("abbrev_zefix \nsubuserdata:".anytext($subuserdata)."\nouresult:".anytext($ouresult),1);
		if ($subuserdata["abbreviation_id"]!="") {
			if ($ouresult["ou_abbreviation"]=="") {
				$setabbrev=", ou_abbreviation='".SQLite3::escapeString(strtoupper($subuserdata["abbreviation_id"]))."'";
				$tmpslug=substr(getslugpart($subuserdata["abbreviation_id"]),0,db_get_config($db,"general:use_abbreviationfield"));
				if (getconfig("slug_include_city")===true) {
					$tmpslug.=".".getslugpart($ouresult["ou_address_city"],"_",false);
				}
				$ouresult["ou_abbreviation"]=strtoupper($subuserdata["abbreviation_id"]);
				mib_sendcommand("/admin/mail/aliases/add","POST",array("update_if_exists" => 0,"address" => $tmpslug."@".getconfig("maildomain"), "forwards_to" => $mainaddress,"permitted_senders" => ""));
			} else { 
				debuglog($mainaddress.": ignoring abbreviation; already set",2);
			}
		}
		if ($ouresult["ou_abbreviation"]!="") {
			$tmpslug=getslugpart($ouresult["ou_abbreviation"]);
			if (getconfig("slug_include_city")===true) {
				$tmpslug.=".".getslugpart($ouresult["ou_address_city"],"_",false);
			}
			$mailvars["aliasaddresses"].=" ".$tmpslug;
		}
		$mailvars["aliasaddresses"]=str_replace(" ","; ",trim($mailvars["aliasaddresses"]));
		if ($mailvars["aliasaddresses"]=="") { $mailvars["aliasaddresses"]="-"; }
		$cmd="update sl_orgunit set ou_personal_name='".SQLite3::escapeString($subuserdata["username_id"])."',".
															 "ou_personal_email='".SQLite3::escapeString($subuserdata["useremail_id"])."',".
															 "ou_personal_phone='".SQLite3::escapeString($subuserdata["userphone_id"])."'".
															 //"ou_invitecode=''".
															 //$setabbrev.
															 " where ou_id=".($suboudata["ou_id"]*1).";";
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage($mainaddress.": unable to update user in DB ($cmd) ",$db->lastErrorMsg()); die(); }
		$roottasks=array();
		// === (root) change forwarding
		$roottasks[]=array("task" => "change_forwarding","parameters" => array("ou_id" => ($suboudata["ou_id"]*1), "newforward" => $subuserdata["useremail_id"]));
		$mailvars["personname"]=$subuserdata["username_id"];
		// === (root) notify new user (personal address)
		$roottasks[]=array("task" => "mail_template","parameters" => array("template" => "success", "params" => array("to" => $subuserdata["useremail_id"],"vars" => $mailvars)));
		// === (root) notify new user (new address)
		$roottasks[]=array("task" => "mail_template","parameters" => array("template" => "success", "params" => array("to" => $mailvars["mainaddress"],"vars" => $mailvars)));
		$rtid=time().".".random_str(16);
		
		// reset user
		db_reset_nc_user($mainaddress);

		db_set_single_entry($db,"storage","roottasks:".$rtid,serialize($roottasks)); 
		debuglog($mainaddress.": root tasks prepared",5);
		db_set_single_entry($db,"storage","signup:".$subid.":status","confirmed"); 
		db_set_single_entry($db,"storage","signup:".$subid.":lastupdate",time());
		webif_single("#:html:_success_web#","#:html:_success_signup_info#"); 
		die();
	}
	
	
	
	
	
	
	// verify NC authentication & permissions
	if (!isset($_COOKIE['nc_token'])) { backtoauth("no NC token cookie"); }
	$cookietoken=$_COOKIE['nc_token'];
	if (!isset($_COOKIE['nc_username'])) { backtoauth("no NC username cookie"); }
	$cookieuser=$_COOKIE['nc_username'];
	$where="where userid='".SQLite3::escapeString($cookieuser)."' and appid='login_token' and configkey='".SQLite3::escapeString($cookietoken)."';";
	$sstr1=db_get_single_entryC($dbNC,"oc_preferences",$where);
	if ($sstr1===null) { backtoauth("NC token cookie not in database for user ".$cookieuser); }
	$where="where uid='".SQLite3::escapeString($cookieuser)."' and (gid='admin' or gid='admin_schulbox');";
	$sstr2=db_get_single_entryC($dbNC,"oc_group_user",$where);
	if ($sstr2===null) { backtoauth("NC user ".$cookieuser." not in group admin_schulbox or admin"); }
	//echo "<pre>"; print_r($sstr2); echo "</pre>";
	//phpinfo();
	if (!isajax()) {
		webif_main($cookieuser);
	} else {
		ajax_main($cookieuser);
	}
}
die();

function generate_passwordresetlink($db,$mainaddress) {
	$passwordtoken=random_str(64);
	$passworddetails=array("userid" => $mainaddress,"created" => time());
	db_set_single_entry($db,"storage","passwordtoken:".$passwordtoken,serialize($passworddetails));
	debuglog($mainaddress.": token for password change created",5);
	$passwordlink="https://".getconfig("mailhost")."/cloud/schulbox.php?pwreset=".$passwordtoken;
	return $passwordlink;
}


function isajax() {
	return (getvar("ajax")!==null);
}

function respondajax($message,$iserror=false,$data=null) {
	$rstr=array("error" => $iserror,"message" => replacei8n( $message),"data" => $data);
	echo json_encode($rstr);
}

function backtoauth($message) {
	if (!isajax()) {
		echo "<script>top.location.href=\"/cloud/index.php/login\";</script>";
	} else {
		respondajax("Login required:".$message,true,null);
	}
}

/*function rentest() {
	$dbNC = new SQLite3(getconfig("datapath")."/owncloud/owncloud.db");
	$dbRC = new SQLite3(getconfig("datapath")."/mail/roundcube/roundcube.sqlite");
	$dbMI = new SQLite3(getconfig("datapath")."/mail/users.sqlite");
	
	$olduser="testo";
	$newuser="testx";
	
	$olduserfq=$olduser."@".getconfig("maildomain");
	$newuserfq=$newuser."@".getconfig("maildomain");
	// roundcube: db
	$cmd="update users set username='".SQLite3::escapeString($newuserfq)."' where username='".SQLite3::escapeString($olduserfq)."';";
	$res=db_exec($dbRC,$cmd);
	if (!$res) { errormessage("unable to rename user in RC ",$dbRC->lastErrorMsg()); die(); }
	// mail: files
	rename(getconfig("datapath")."/mail/mailboxes/".getconfig("maildomain")."/".$olduser,getconfig("datapath")."/mail/mailboxes/".getconfig("maildomain")."/".$newuser);
	
	// nextcloud: oc_users_external
	$cmd="update oc_users_external set uid='".SQLite3::escapeString($newuserfq)."' where uid='".SQLite3::escapeString($olduserfq)."';";
	$res=db_exec($dbRC,$cmd);
	if (!$res) { errormessage("unable to rename user in NC (step 1) ",$dbRC->lastErrorMsg()); die(); }
	// nextcloud: oc_accounts
	$cmd="update oc_accounts set uid='".SQLite3::escapeString($newuserfq)."' where uid='".SQLite3::escapeString($olduserfq)."';";
	$res=db_exec($dbRC,$cmd);
	if (!$res) { errormessage("unable to rename user in NC (step 2) ",$dbRC->lastErrorMsg()); die(); }
	// nextcloud: oc_accounts_data
	$cmd="update oc_accounts_data set uid='".SQLite3::escapeString($newuserfq)."' where uid='".SQLite3::escapeString($olduserfq)."';";
	$res=db_exec($dbRC,$cmd);
	if (!$res) { errormessage("unable to rename user in NC (step 3) ",$dbRC->lastErrorMsg()); die(); }
	// nextcloud: files
	rename(getconfig("datapath")."/owncloud/".$olduserfq,getconfig("datapath")."/owncloud/".$newuserfq);
	// nextcloud rescan files
	exec("sudo -u www-data php /usr/local/lib/owncloud/console.php files:scan --all",$output,$result);
	echo "nc rescan: $result:$output";
	
	
} */


// check for init done


function getmailreplacers($tplname) {
	$replacers=array("confirmurl" => 	 array("templates" => array("confirm"),"value" => "confirmurl", "valsrc" => "vars", "text" => "?"),
									 "confirmtoken" => array("templates" => array("confirm"),"value" => "confirmtoken", "valsrc" => "vars", "text" => "?"),
									 "passwordreseturl" => array("templates" => array("success","pwresetlink"),"value" => "passwordreseturl", "valsrc" => "vars", "text" => "?"),
									 "maillink" =>		 array("templates" => array("success"),"value" => "https://".getconfig("mailhost")."/mail", "valsrc" => "raw", "text" => "?"),
									 "cloudlink" =>		 array("templates" => array("success"),"value" => "https://".getconfig("mailhost")."/cloud", "valsrc" => "raw", "text" => "?"),
									 "sender" =>			 array("templates" => array(),				 "value" => db_get_config(null,"email:sendername"), "valsrc" => "raw", "text" => "?"),
									 "signature" =>		 array("templates" => array(),				 "value" => db_get_config(null,"email:signature"), "valsrc" => "raw", "text" => "?"),
									 "mainaddress" =>	 array("templates" => array("success","handover","pwreset"),"value" => "mainaddress", "valsrc" => "vars", "text" => "?"),
									 "aliasaddresses" =>	 array("templates" => array("success"),"value" => "aliasaddress", "valsrc" => "vars", "text" => "?"),
									 "personaladdress" =>	 array("templates" => array("success"),"value" => "personaladdress", "valsrc" => "vars", "text" => "?"),
									 "personname" =>	 array("templates" => array("success","handover","pwresetlink","pwreset"),"value" => "personname", "valsrc" => "vars", "text" => "?"),
	);
	$replist=array();
	foreach($replacers as $reptoken => $repdata) {
		if (($tplname=="*") || (count($repdata["templates"])==0) || (in_array($tplname,$repdata["templates"]))) {
			$replist[$reptoken]=$repdata;
		}
	}
	return $replist;
}

function change_forwarding($ouid,$newmailaddr) {
	$db=db_getlink("self");
	$dbNC=db_getlink("NC");
	$where="where ou_id=".($ouid*1).";";
	$ouresult=db_get_single_entryC($db,"sl_orgunit",$where);
	if ($ouresult===null) {
		errormessage("! unable to change forwarding for ou $ouid; ou_id not found");
		return false;
	}
	$id=$ouresult["ou_mib_email"];
	
	$spath="/home/user-data/mail/sieve/".getconfig("maildomain");
	$upath="/home/user-data/mail/sieve/".getconfig("maildomain")."/".$ouresult["ou_slug"];
	if (!file_exists($upath)) {
		mkdir($upath);
		chown($upath,"mail");
		chgrp($upath,"mail");
	}
	if (!file_exists($upath."/roundcube.sieve")) {
		touch($upath."/roundcube.sieve");
		chown($upath."/roundcube.sieve","mail");
		chgrp($upath."/roundcube.sieve","mail");
	}
	if (!file_exists($upath.".sieve")) {
		symlink($ouresult["ou_slug"]."/roundcube.sieve",$upath.".sieve");
	}
	$svtext="require [\"copy\"];\n# rule:[Weiterleitung]\nif true\n{\n        redirect :copy \"".$newmailaddr."\";\n}\n";
	$rc=fopen($upath."/roundcube.sieve","w");
	fputs($rc,$svtext);
	fclose($rc);
	debuglog("mail forwarding for $id changed",4);
	
}

function mail_template($tplname, $param ) {
	if (!isset($param["headers"])) { $param["headers"]=array(); }
	$defaultfromaddress=db_get_config(null,"email:senderaccount")."@".getconfig("maildomain");
	if (!isset($param["parameters"])) { $param["parameters"]="-f ".$defaultfromaddress; }
	if (!isset($param["from"])) { $param["from"]=$defaultfromaddress; }
	if (!isset($param["headers"]["From"])) {
		$param["headers"]["From"]='=?UTF-8?B?'.base64_encode(db_get_config(null,"email:sendername")).'?= <'.$param["from"].'>';
	}
	$tpltext=db_get_config(null,"email:".$tplname);
	// replace tokens
	$replacers=getmailreplacers($tplname);
	foreach($replacers as $reptoken => $repdata) {
		$repcount=0;
		$repvalue="%".$reptoken."%";
		//do {
			if ($repdata["valsrc"]=="raw") {
				$repvalue=$repdata["value"];
			}
			if ($repdata["valsrc"]=="vars") {
				if (isset($param["vars"][$reptoken])) {
					$repvalue=$param["vars"][$reptoken];
				} else {
					$repvalue=replacei8n("#:raw- unset -#");
				}
			}

			$oldtpltext=$tpltext; $tpltext=str_replace("%".$reptoken."%",$repvalue,$tpltext); if ($oldtpltext!=$tpltext) { $repcount++; }
		//} while($repcount > 0);
	}
	$subject=replacei8n("#:raw:_email:".$tplname."#");
	if (isset($param["subject"])) { $subject=$param["subject"]; }
	if (!isset($param["to"])) {
		errormessage("! need recipient to send mail, bu no recipient given"); die();
	}
	// replace (subject) from template
	$validreplace=array("subject","bcc","cc");
	$tplx=explode("\n",$tpltext);
	$param["subject"] = $subject;
	foreach($tplx as $tplid => $tpline) {
		if (substr($tpline,0,3)=="---") {
			$tptest=substr($tpline,3);
			$tptestx=explode(":",$tptest,2);
			//echo anytext($tptestx);
			if (count($tptestx)==2) {
				foreach($validreplace as $vr) {
					//echo "vr:".anytext($vr).":".strtolower($tptestx[0])."\n";
					if (strtolower($tptestx[0])==$vr) {
						$param[$vr]=$tptestx[1];
						unset($tplx[$tplid]);
					}
				}
			}
		}
	}
	if (isset($param["bcc"])) { $param["headers"]["bcc"]=$param["bcc"]; }
	if (isset($param["cc"])) { $param["headers"]["cc"]=$param["cc"]; }
	$param["message"]=join("\n",$tplx);
	$param["subject_raw"] = $param["subject"];
	$param["subject"] = '=?UTF-8?B?'.base64_encode($param["subject"]).'?=';
	//$param["from_raw"] = $param["headers"]["From"];
	//$param["headers"]["From"]='=?UTF-8?B?'.base64_encode($param["headers"]["From"]).'?=';
	//echo "param:".anytext($param);
	$result=mail( $param["to"], $param["subject"], $param["message"], $param["headers"], $param["parameters"] );
	$fn="mail_".$tplname."_".time()."_".random_str(8);
	$rc=fopen("/tmp/".$fn,"w");
	fputs($rc,"tpl:".$tplname."\n");
	fputs($rc,"result:".$result."\n");
	fputs($rc,"param:".anytext($param)."\n");
	fclose($rc);
	debuglog("template ".$tplname." sent to ".$param["to"]." ".$fn,5);

}



function getconfig($item=false) {
	global $config;
	if ($item===false) { return $config; }
	if (isset($config[$item])) {
		return $config[$item];
	}
	return null;
}


function setconfig($item,$value) {
	global $config;
	$config[$item]=$value;
}




// ============================================================================
//
// database functions
//
// ============================================================================


function getslugpart($name,$space="-",$umlautok=false) {
$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'Ae', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'Ue', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'ae', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'oe', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y',
                            'Ğ'=>'G', 'İ'=>'I', 'Ş'=>'S', 'ğ'=>'g', 'ı'=>'i', 'ş'=>'s', 'ü'=>'ue',
                            'ă'=>'a', 'Ă'=>'A', 'ș'=>'s', 'Ș'=>'S', 'ț'=>'t', 'Ț'=>'T' );

$pregstr="A-Za-z0-9 \-";

if ($umlautok) {
	$unwanted_array["Ä"] = "ä";
	$unwanted_array["Ö"] = "ö";
	$unwanted_array["Ü"] = "ü";
	$unwanted_array["ä"] = "ä";
	$unwanted_array["ö"] = "ö";
	$unwanted_array["ü"] = "ü";
	$unwanted_array["ß"] = "ß";
	$pregstr.="äöüß";
}
$str = strtolower(strtr( $name, $unwanted_array ));
$str=preg_replace("/[\/\t]/", ' ', $str);
$str=preg_replace("/[^".$pregstr."]/", '', $str);
while (strpos($str,"  ")!==false) {
	$str=str_replace("  "," ",$str);
}
$str=trim($str);
$str=str_replace(" ",$space,$str);
$str=preg_replace("/--/", '-', $str);
return $str;
}

function slug_from_ou($ou,$umlautok=false,$usefield="name") {
	if (!isset($ou["name"])) { return false; }
	if (getconfig("slug_include_city")===true) { if (!isset($ou["address_city"])) { return false; } }
	$tmpsl1=getslugpart($ou[$usefield],"-",$umlautok);
	$tmpsl2=getslugpart($ou["address_city"],"_",$umlautok);
	$maxlen=64;
	if ((getconfig("slug_prefix")!=="")) { $maxlen-=strlen(getconfig("slug_prefix")); }
	if ((getconfig("slug_appendix")!=="")) { $maxlen-=strlen(getconfig("slug_appendix")); }
	if (getconfig("slug_include_city")===true) {
		$tmpslug=$tmpsl1.".".$tmpsl2;
		if (strlen($tmpslug)>$maxlen) {
			$tmpslug=substr($tmpsl1,0,($maxlen-(strlen($tmpsl2)+1))).".".$tmpsl2;
		}
	} else {
		$tmpslug=$tmpsl1;
		if (strlen($tmpslug)>$maxlen) { $tmpslug=substr($tmpslug,0,$maxlen); }
	}
	if ((getconfig("slug_prefix")!=="")) { $tmpslug=getconfig("slug_prefix").$tmpslug; }
	if ((getconfig("slug_appendix")!=="")) { $tmpslug=$tmpslug.getconfig("slug_appendix"); }
	return $tmpslug;
}

function db_getlink($which) {
	$dbfile=db_getfile($which);
	if ($dbfile===false) { return false; }
	$db = new SQLite3($dbfile); 
	return $db;
}
function db_getfile($which) {
	if ($which=="NC") { return getconfig("datapath")."/../owncloud/owncloud.db"; }
	if ($which=="RC") { return getconfig("datapath")."/../mail/roundcube/roundcube.sqlite"; }
	if ($which=="MI") { return getconfig("datapath")."/../mail/users.sqlite"; }
	if ($which=="self") {return getconfig("database"); }
	if ($which=="LOG") {return getconfig("datapath")."/schulelog.db"; }
	return false;
}

function db_checkdbs() {
	$dblist=array("NC","RC","MI","self","LOG");
	$res="";
	foreach($dblist as $dbwhich) {
		$dbfile=db_getfile($dbwhich);
		if (!$dbfile) { $res.=$dbwhich.":no_dbinfo "; } else {
			if (!file_exists($dbfile)) 	{ $res.=$dbwhich.":no_file "; } 
		}
	}
	return ($res==="");
}

function db_generate_invitecode($db=null) {
	if ($db===null) { $db=db_getlink("self"); }
	do {
		$rb=random_bytes(6);
		$rst="";
		for($i=0;$i<strlen($rb);$i++) {
			$r=ord(substr($rb,$i,1));
			if ($r < 16) { $r="0".dechex($r); } else { $r="".dechex($r); }
			$rst.=$r;
		}
		$where="where ou_invitecode='".SQLite3::escapeString($rst)."'";
		$checkentry=db_get_single_entryC($db,"sl_orgunit",$where);
	} while($checkentry!==null);
	return $rst;
}
function db_add_ou($db,$ou) {
	if (!isset($ou["name"])) { return false; }
	if (getconfig("slug_include_city")===true) { if (!isset($ou["address_city"])) { return false; } }
	$ou_tableinfo=db_query($db,"PRAGMA table_info(sl_orgunit)");//"SELECT * FROM sqlite_master"); // WHERE type='table' AND name='sl_orgunit';");
	$cdata=array();
	foreach($ou_tableinfo as $tid => $rowinfo) {
		$rowname=substr($rowinfo["name"],3);
		if ($rowname!="id") {
			if (isset($ou[$rowname])) {
				$cdata["ou_".$rowname]=$ou[$rowname];
			} else {
				$cdata["ou_".$rowname]="";
			}
		}
	}
	
	if (isset($cdata["ou_categories"]) && (is_array($cdata["ou_categories"]))) { $cdata["ou_categories"]=json_encode($cdata["ou_categories"]); }
	if (isset($cdata["ou_custominfo"]) && (is_array($cdata["ou_custominfo"]))) { $cdata["ou_custominfo"]=json_encode($cdata["ou_custominfo"]); }
	if (isset($cdata["ou_parameters"]) && (is_array($cdata["ou_parameters"]))) { $cdata["ou_parameters"]=json_encode($cdata["ou_parameters"]); }
	$slug=slug_from_ou($ou);
	$cdata["ou_invitecode"]=db_generate_invitecode($db);
	$slugalias=slug_from_ou($ou,true);
	$cdata["ou_slug"]=$slug;
	$cdkeys=array(); $cdvals=array();
	$mailaddress=$cdata["ou_slug"]."@".getconfig("maildomain");
	
	$cdata["ou_mib_email"]=$mailaddress;
	
	foreach($cdata as $cdkey => $cdval) {
		$cdkeys[]="'".SQLite3::escapeString($cdkey)."'";
		$cdvals[]="'".SQLite3::escapeString($cdval)."'";
	}
	$where="where ou_slug='".SQLite3::escapeString($slug)."'";
	$checkentry=db_get_single_entryC($db,"sl_orgunit",$where);
	if ($checkentry!==null) {
		errormessage("user ".$slug." already exists");
		return false;
	}
	stdlog("* creating user: ".$slug." ic:".$cdata["ou_invitecode"]);
	$cmd="insert into sl_orgunit(".implode(",",$cdkeys).") values(".implode(",\n",$cdvals).");";
	$res=db_exec($db,$cmd);
	if (!$res) { errormessage("! unable to insert OU into database",$db->lastErrorMsg()); die(); }
	$cit=db_get_config($db,"general:custominfo_items");
	//echo "cit:".anytext($cit)."\n";
	$accesstoken=db_get_config($db,"volatile:mib_accesstoken");
	//echo "accesstoken:".anytext($accesstoken)."\n";
	// create user
	$userpassword=randompassword();
	$mibcreate=mib_sendcommand("/admin/mail/users/add","POST",array("email" => $mailaddress,"password" => $userpassword,"privileges" => ""));
	// add aliases
	$aliaslist=array();
	if (isset($cdata["ou_mib_email_aliases"]) && ($cdata["ou_mib_email_aliases"]!="")) {
		$aliaslist=explode(" ",$cdata["ou_mib_email_aliases"]);
	}
	if (isset($cdata["ou_abbreviation"]) && ($cdata["ou_abbreviation"]!="")) {
		$tmpslug=getslugpart($cdata["ou_abbreviation"]);
		if (getconfig("slug_include_city")===true) {
			$tmpslug.=".".getslugpart($cdata["ou_address_city"],"_",false);
		} 
		$aliaslist[]=$tmpslug."@".getconfig("maildomain");
	}
	debuglog("aliaslist: ".anytext($aliaslist),8);
	foreach($aliaslist as $alias) {
		if (trim($alias)!="") {
			//echo $alias."\n";
			mib_sendcommand("/admin/mail/aliases/add","POST",array("update_if_exists" => 0,"address" => $alias, "forwards_to" => $mailaddress,"permitted_senders" => ""));
		}
	}
	debuglog($mailaddress.": user created in mib: ".anytext($mibcreate["body"]));
	// update databases
	$dbNC = new SQLite3(getconfig("datapath")."/../owncloud/owncloud.db");
	$dbRC = new SQLite3(getconfig("datapath")."/../mail/roundcube/roundcube.sqlite");
	$dbMI = new SQLite3(getconfig("datapath")."/../mail/users.sqlite");
	$mailusername=db_get_config($db,"general:ou_description")." ".$cdata["ou_name"];
	$mailuseraccount=$cdata["ou_slug"]."@".getconfig("maildomain");
	$mailuseraddress=$cdata["ou_name"]."\n".$cdata["ou_address_street"]."\n".$cdata["ou_address_postcode"]." ".$cdata["ou_address_city"];
	$mailuserorga=$cdata["ou_address_city"];

	rc_simulate_login($mailuseraccount,$userpassword);

	// RoundCube
	$rcid=db_get_single_entryC($dbRC,"users","where username='".SQLite3::escapeString($mailuseraccount)."'");
	//debuglog("rcid:".anytext($rcid),8);
	if ($rcid===null) {
		errormessage("error updating RC for user ".$cdata["ou_slug"],"searching rcid returned ".anytext($rcid));
	}
	$rcuserid=$rcid["user_id"];
	$cmd="update identities set name='".SQLite3::escapeString($mailusername)."', organization='".SQLite3::escapeString($mailuserorga)."' where user_id=".$rcuserid;
	$res=db_exec($dbRC,$cmd);
	if (!$res) { errormessage("error updating RC for user ".$cdata["ou_slug"],$db->lastErrorMsg()); die(); }
	debuglog($mailaddress.": roundcube DB updated",3);
	// Nextcloud
	//$odebugl=getconfig("debuglevel");
	//setconfig("debuglevel",8);
	nc_simulate_login($mailuseraccount,$userpassword);
	//setconfig("debuglevel",$odebugl);
	$ncid=db_get_single_entryC($dbNC,"oc_users_external","where uid='".SQLite3::escapeString($mailuseraccount)."'");
	//debuglog("ncid:".anytext($ncid),5);
	if ($ncid===null) {
		errormessage("error updating NC for user ".$cdata["ou_slug"],"searching users_external returned ".anytext($ncid));
	}
	/*$cmd="update oc_accounts_data set value='".SQLite3::escapeString($mailusername)."' where uid='".SQLite3::escapeString($mailuseraccount)."' and name='displayname'";
	$res=db_exec($dbNC,$cmd);
	if (!$res) { errormessage("error updating NC for user ".$cdata["ou_slug"],$db->lastErrorMsg()); die(); }
	$cmd="update oc_accounts_data set value='".SQLite3::escapeString($mailuserorga)."' where uid='".SQLite3::escapeString($mailuseraccount)."' and name='organization'";
	$res=db_exec($dbNC,$cmd);
	if (!$res) { errormessage("error updating NC for user ".$cdata["ou_slug"],$db->lastErrorMsg()); die(); }
	$cmd="update oc_accounts_data set value='".SQLite3::escapeString($mailuseraddress)."' where uid='".SQLite3::escapeString($mailuseraccount)."' and name='address'";
	$res=db_exec($dbNC,$cmd);
	if (!$res) { errormessage("error updating NC for user ".$cdata["ou_slug"],$db->lastErrorMsg()); die(); }
	$ncjr=db_get_single_entryC($dbNC,"oc_accounts","where uid='".SQLite3::escapeString($mailuseraccount)."'");
	if ($ncjr===null) {
		errormessage("error updating NC for user ".$cdata["ou_slug"],"searching accounts returned ".anytext($ncjr));
	}
	$ncj=json_decode($ncjr["data"],true);
	$ncj["displayname"]["value"]=$mailusername;
	$ncj["address"]["value"]=$mailuseraddress;
	$ncj["organisation"]["value"]=$mailuserorga;
	//print_r($ncj);
	$cmd="update oc_accounts set data='".SQLite3::escapeString(json_encode($ncj))."' where uid='".SQLite3::escapeString($mailuseraccount)."'";
	$res=db_exec($dbNC,$cmd);
	if (!$res) { errormessage("error updating NC for user ".$cdata["ou_slug"],$db->lastErrorMsg()); die(); } */
	
	$res1=db_update_nc_userfield($dbNC,$mailuseraccount,"displayname",$mailusername,true) ;
	$res2=db_update_nc_userfield($dbNC,$mailuseraccount,"organization",$mailuserorga,true) ;
	$res3=db_update_nc_userfield($dbNC,$mailuseraccount,"organisation",$mailuserorga,true) ;
	$res4=db_update_nc_userfield($dbNC,$mailuseraccount,"address",$mailuseraddress,true) ;
	$res5=db_update_nc_userfield($dbNC,$mailuseraccount,"email",$mailuseraccount,true) ;
	db_reset_nc_user($mailuseraccount);
	debuglog($mailaddress.": nextcloud DB updated",3);

	/*
	print_r($cdata);
	print_r($ncid);
	*/
}


function db_reset_nc_user($ncuid) {
	$dbNC=db_getlink("NC");
	// remove authtokens
	$cmd="delete from oc_authtoken where uid='".SQLite3::escapeString($ncuid)."'";
	$res=db_exec($dbNC,$cmd);
	if (!$res) { errormessage($ncuid.": unable to revoke authtokens in NC ($cmd) ",$db->lastErrorMsg()); die(); }
	debuglog($ncuid.": authtokens revoked",3);
	
	// set privacy preferences
	$pprefs=json_decode('{"email":{"visibility":"show_users_only"},"address":{"visibility":"show_users_only"},"avatar":{"visibility":"show_users_only"},"biography":{"visibility":"show_users_only"},"displayname":{"visibility":"show_users_only"},"headline":{"visibility":"show_users_only"},"organisation":{"visibility":"show_users_only"},"role":{"visibility":"show_users_only"},"phone":{"visibility":"show_users_only"},"twitter":{"visibility":"show_users_only"},"website":{"visibility":"show_users_only"}}',true);
	$cmd="select * from oc_profile_config where user_id='".SQLite3::escapeString($ncuid)."'";
	$res=db_query($dbNC,$cmd);
	debuglog("cmd:$cmd\nres:".anytext($res),8);
	$cmd2="";
	if (($res!==null) && (count($res)>0)) {
		$xprefs=json_decode($res[0]["config"],true);
		$pprefs=array_merge($xprefs,$pprefs);
		//if ($res===null) {
			$cmd2="update oc_profile_config set config='".SQLite3::escapeString(json_encode($pprefs))."' where user_id='".SQLite3::escapeString($ncuid)."'";
		//}
	} else {
		$cmd2="insert into oc_profile_config (user_id,config) values=('".SQLite3::escapeString($ncuid)."','".SQLite3::escapeString(json_encode($pprefs))."')";
	}
	if ($cmd2!="") {
		$res2=db_query($dbNC,$cmd2);
		debuglog("cmd2:$cmd2\nres:".anytext($res2),8);
	}
	debuglog($ncuid.": privacy configuration reset",3);
	
	/*
		$cmd="delete from oc_authtoken where uid='".SQLite3::escapeString($mainaddress)."'";
		$res=db_exec($dbNC,$cmd);
		if (!$res) { errormessage($mainaddress.": unable to revoke authtokens in NC ($cmd) ",$db->lastErrorMsg()); die(); }
	*/
}


function db_update_nc_userfield($dbNC,$uid,$fieldname,$fieldvalue,$createifnotexists=true) {
	if ($dbNC==null) { $dbNC=db_getlink("NC"); }
	$cdata=db_get_single_entryC($dbNC,"oc_accounts_data","where uid='".SQLite3::escapeString($uid)."' and name='".SQLite3::escapeString($fieldvalue)."'");
	$cmd="";
	if ($cdata===null) {
		if ($createifnotexists) {
			$cmd="insert into oc_accounts_data (uid,name,value) values('".SQLite3::escapeString($uid)."','".SQLite3::escapeString($fieldname)."','".SQLite3::escapeString($fieldvalue)."')";
		}
	} else {
		$cmd="update oc_accounts_data set value='".SQLite3::escapeString($fieldvalue)."' where uid='".SQLite3::escapeString($uid)."' and name='".SQLite3::escapeString($fieldname)."'";
	}
	if ($cmd!="") {
		$res=db_exec($dbNC,$cmd);
		if (!$res) { errormessage("error updating NC oc_accounts_data user:".$uid." fieldname:$fieldname fieldvalue:$fieldvalue cmd:$cmd\nquery:".anytext($cdata)."\nresult:".anytext($res)."\n".$dbNC->lastErrorMsg()); }
	}
	debuglog("updating NC oc_accounts_data user:".$uid." fieldname:$fieldname fieldvalue:$fieldvalue cmd:$cmd\nquery:".anytext($cdata)."\nresult:".anytext($res)."\n".$dbNC->lastErrorMsg(),8); 
	$cmd="";
	$cdata=db_get_single_entryC($dbNC,"oc_accounts","where uid='".SQLite3::escapeString($uid)."'");
	if ($cdata===null) {
		if ($createifnotexists) {
			$cdataj[$fieldname]["value"]=$fieldvalue;
			$cmd="insert into oc_accounts (uid,data) values('".SQLite3::escapeString($uid)."','".SQLite3::escapeString(json_encode($cdataj))."')";
		}
	} else {
		$cdataj=json_decode($cdata["data"],true);
		$cdataj[$fieldname]["value"]=$fieldvalue;
		$cmd="update oc_accounts set data='".SQLite3::escapeString(json_encode($cdataj))."' where uid='".SQLite3::escapeString($uid)."'";
	}
	if ($cmd!="") {
		$res=db_exec($dbNC,$cmd);
		if (!$res) { errormessage("error updating NC oc_accounts user:".$uid." fieldname:$fieldname fieldvalue:$fieldvalue cmd:$cmd\nquery:".anytext($cdata)."\nresult:".anytext($res)."\n".$dbNC->lastErrorMsg()); }
	}
	debuglog("updating NC oc_accounts user:".$uid." fieldname:$fieldname fieldvalue:$fieldvalue cmd:$cmd\nquery:".anytext($cdata)."\nresult:".anytext($res)."\n".$dbNC->lastErrorMsg(),8); 
	return true;
}





function db_update_schema($db) {
	$fromversion=false;
	if (db_table_exists($db,"sl_versioninfo")) {
		$version=db_query($db,"select * from sl_versioninfo where version_key='version';");
		$fromversion=$version[0]["version_value"];
		echo "- running version: ".$fromversion."\n";
	}
	db_migrate($db,$fromversion);
}

function db_update_RC_account($dbRC,$which,$what,$createifnotexists=false) {
	// $which=array    user_id/username => 
	if (!isset($which["user_id"]) && !isset($which["username"])) {
		return null;
	}
	foreach($which as $whfield => $whvalue) {
		$condition=$whfield."='".SQLite3::escapeString($whvalue)."'";
		$conditionwhatfield=$whfield;
		$conditionwhatvalue=$whvalue;
	}
	$rcusers=db_get_single_entryC($dbRC,"users","where ".$condition);
/*	if (!$rcusers && !$createifnotexists) {
		return false;
	}
	if (!$rcusers) {
		if (!isset($what["created"])) { $what["created"]=date("Y-m-d H:i:s"); }
		if (!isset($what["language"])) { $what["language"]="DE_de"; }
		if (!isset($what["preferences"])) { $what["preferences"]=serialize(array()); }
		if (!isset($what["mail_host"])) { $what["mail_host"]="local"; }
		$cmd="INSERT INTO users(username,mail_host,created,language,preferences) VALUES('".SQLite3::escapeString($conditionwhatvalue)."',".
																																									 "'".SQLite3::escapeString($what["mail_host"])."',".
																																									 "'".SQLite3::escapeString($what["created"])."',".
																																									 "'".SQLite3::escapeString($what["language"])."',".
																																									 "'".SQLite3::escapeString($what["preferences"])."'";
	} */
	if (!$rcusers) {
		return false;
	}
	$rcuserid=$rcid["user_id"];
	$cmd="update identities set name='".SQLite3::escapeString($what["mailusername"])."', organization='".SQLite3::escapeString($what["mailuserorga"])."' where user_id=".$rcuserid;
	$res=db_exec($dbRC,$cmd);
	if (!$res) { errormessage("error updating RC for user $conditionwhatfield=$conditionwhatvalue",$db->lastErrorMsg()); die(); }
	return true;
}

function db_migrate($db,$fromversion) {
	$currentversion="4";
	$dbLOG=db_getlink("LOG");
	if ($fromversion==$currentversion) { return true; }
	if ($fromversion===false) { // no version found or no db created
		echo "- database empty; creating schema; version ".$currentversion."\n";
		$cmd=db_table_schema("sl_versioninfo");
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to create table sl_versioninfo",$db->lastErrorMsg()); die(); }
		$cmd="INSERT INTO sl_versioninfo(version_key,version_value) VALUES('version','".$currentversion."');";
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to update table sl_versioninfo",$db->lastErrorMsg()); die(); }
		$cmd=db_table_schema("sl_config");
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to create table sl_config",$db->lastErrorMsg()); die(); }
		$cmd=db_table_schema("sl_storage");
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to create table sl_storage",$db->lastErrorMsg()); die(); }
		$cmd=db_table_schema("sl_orgunit");
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to create table sl_orgunit",$db->lastErrorMsg()); die(); }
		db_set_config_defaults($db);
	}
	if ($fromversion=="1") {
		$thisversion="2";
		echo "- upgrading to schema version ".$thisversion."\n";
		$cmd=db_table_schema("sl_config");
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to create table sl_config",$db->lastErrorMsg()); die(); }
		$cmd="UPDATE sl_versioninfo set version_value='".$thisversion."' where version_key='version';";
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to update version information",$db->lastErrorMsg()); die(); }
		$fromversion=$thisversion;
		db_set_config_defaults($db);
		
	}
	if ($fromversion=="2") {
		$thisversion="3";
		echo "- upgrading to schema version ".$thisversion."\n";
		$cmd=db_table_schema("sl_orgunit");
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to create table sl_orgunit",$db->lastErrorMsg()); die(); }
		$cmd="UPDATE sl_versioninfo set version_value='".$thisversion."' where version_key='version';";
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to update version information",$db->lastErrorMsg()); die(); }
		$fromversion=$thisversion;
		db_set_config_defaults($db);
		
	}
	if ($fromversion=="3") {
		$thisversion="4";
		echo "- upgrading to schema version ".$thisversion."\n";
		$cmd=db_table_schema("sl_storage");
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to create table sl_storage",$db->lastErrorMsg()); die(); }
		$cmd="UPDATE sl_versioninfo set version_value='".$thisversion."' where version_key='version';";
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage("unable to update version information",$db->lastErrorMsg()); die(); }
		$fromversion=$thisversion;
		db_set_config_defaults($db);
		
	}
}

function db_table_schema($tablename) {
	if ($tablename=="sl_versioninfo") { return "CREATE TABLE IF NOT EXISTS ".$tablename." ( version_key TEXT PRIMARY KEY, version_value TEXT)"; }
	if ($tablename=="sl_config") { return "CREATE TABLE IF NOT EXISTS ".$tablename." ( config_key TEXT PRIMARY KEY, config_value TEXT)"; }
	if ($tablename=="sl_storage") { return "CREATE TABLE IF NOT EXISTS ".$tablename." ( storage_key TEXT PRIMARY KEY, storage_value TEXT)"; }
	if ($tablename=="sx_log")     { return "CREATE TABLE IF NOT EXISTS ".$tablename." ( log_id INTEGER PRIMARY KEY AUTOINCREMENT, log_datetime REAL, log_pid INTEGER, log_severity INTEGER, log_text TEXT)"; }
	if ($tablename=="sl_orgunit") { return "CREATE TABLE IF NOT EXISTS ".$tablename." ( ou_id INTEGER PRIMARY KEY, ou_name TEXT NOT NULL, ou_abbreviation TEXT, ou_slug TEXT, ou_external_id TEXT, ou_address_street TEXT, ou_address_city TEXT, ou_address_postcode TEXT,  ou_categories TEXT, ou_mib_email TEXT, ou_email TEXT, ou_invitecode TEXT, ou_mib_email_aliases TEXT, ou_custominfo TEXT, ou_parameters TEXT, ou_personal_name TEXT,ou_personal_email TEXT,ou_personal_phone TEXT)"; }
}


function db_get_config($db,$item) {
	if ($db===null) { $db=db_getlink("self"); }
	return db_get_single_entry($db,"config",$item);
}

function db_set_config($db,$item,$value) {
	if ($db===null) { $db=db_getlink("self"); }
	$old=db_get_config($db,$item);
	if ($old===null) {
		$cmd="INSERT INTO sl_config (config_key,config_value) VALUES('".SQLite3::escapeString($item)."','".SQLite3::escapeString($value)."');";
	} else {
		$cmd="UPDATE sl_config set config_value='".SQLite3::escapeString($value)."' where config_key='".SQLite3::escapeString($item)."';";
	}
	$res=db_exec($db,$cmd);
	//echo "<pre>".$cmd."\n".anytext($res)."</pre>";
	return $res;
}

function db_set_single_entry($db,$tableshort,$item,$value) {
	if ($db===null) { $db=db_getlink("self"); }
	$old=db_get_single_entry($db,$tableshort,$item);
	if ($old===null) {
		$cmd="INSERT INTO sl_".$tableshort." (".$tableshort."_key,".$tableshort."_value) VALUES('".SQLite3::escapeString($item)."','".SQLite3::escapeString($value)."');";
	} else {
		$cmd="UPDATE sl_".$tableshort." set ".$tableshort."_value='".SQLite3::escapeString($value)."' where ".$tableshort."_key='".SQLite3::escapeString($item)."';";
	}
	$res=db_exec($db,$cmd);
	//echo "<pre>".$cmd."\n".anytext($res)."</pre>";
	return $res;
}

/*function db_set_entry($db,$tblname,$conditions,$setvalues) {
	$condarray="";
	foreach($conditions as $condvar => $condval) {
		$condarray[]=$condvar." ='".SQLite3::escapeString($condval)."' ";
	}
	$condstring=join("AND",$condarray);
	$cmd="select * from ".$tblname." where ".$condstring;
	$pq=db_query($db,$cmd);
	if (count($pq)>0) {
		foreach($setvalues)
		$cmd="INSERT INTO ".$tblname." (config_key,config_value) VALUES('".SQLite3::escapeString($defkey)."','".SQLite3::escapeString($defval)."');";
		} else {
			if ($force) {
				$cmd="UPDATE sl_config set config_value='".SQLite3::escapeString($defval)."' where config_key='".SQLite3::escapeString($defkey)."');";
} */

function db_get_single_entry($db,$tableshort,$item) {
	$keycol=$tableshort."_key";
	$valcol=$tableshort."_value";
	$tblname="sl_".$tableshort;
	$cmd="select ".$keycol.",".$valcol." from ".$tblname." where ".$keycol."='".SQLite3::escapeString($item)."'";
	$se=db_query($db,$cmd);
	// print_r($se);
	if (isset($se[0][$valcol])) {
		return $se[0][$valcol];
	} else {
		return null;
	}
}

function db_get_single_entryC($db,$table,$customwhere) {
	$cmd="select * from ".$table." ".$customwhere;
	$se=db_query($db,$cmd);
	debuglog("cmd: $cmd\n".anytext($se),9);
	if (isset($se[0])) {
		return $se[0];
	} else {
		return null;
	}
}

function db_get_single_entryC_or($db,$table,$customwhere,$fieldnametoreturn,$returnifnotfound=false) {
	$cmd="select * from ".$table." ".$customwhere;
	$se=db_query($db,$cmd);
	debuglog("cmd: $cmd\n".anytext($se),9);
	if (isset($se[0])) {
		if (isset($se[0][$fieldnametoreturn])) {
			return $se[0][$fieldnametoreturn];
		} else {
			return $returnifnotfound;
		}
	} else {
		return $returnifnotfound;
	}
}


function db_get_multi_entryC($db,$table,$customwhere) {
	$cmd="select * from ".$table." ".$customwhere;
	$se=db_query($db,$cmd);
	debuglog("cmd: $cmd\n".anytext($se),9);
	if (isset($se)) {
		return $se;
	} else {
		return null;
	}
}


function db_set_config_defaults($db,$force=false) {
	$cmd="select * from sl_config;";
	$dconfig=db_query($db,$cmd);
	
	$catitems=array("landkreis" => array("desc" => "Landkreis", "type" => "text"),
										"schulart" => array("desc" => "Schulart", "type" => "select", "select" => array("Gymnasium","Werkrealschule","Grundschule")),
										"schultraeger" => array("desc" => "Schulträger", "type" => "text"),
										"aktiv" => array("desc" => "Aktiv", "type" => "text"));
	$cuiitems=array("Schulgröße" => array("desc" => "Schulgröße", "type" => "text"));
	
	if ($dconfig===false) { errormessage("unable to read sl_config",$db->lastErrorMsg()); die(); }
	$defaults=array("form_signup:id" => "","form_signup:field_invitecode_id" => "","form_signup:field_username_id" => "","form_signup:field_useremail_id" => "","form_signup:field_userphone_id" => "","form_signup:field_abbreviation_id" => "",
									"general:ou_description" => "SMV", "general:categories_items" => json_encode($catitems), "general:custominfo_items" => json_encode($cuiitems),"general:use_abbreviationfield" => "3",
									"email:confirm" => "Alles okay.\nBitte klicke hier %confirmurl% um zu bestätigen. (%confirmtoken%)\n\n%sender%\n%signature%",
									"email:confirm_invalidcode" => "---Subject:Falscher Fehler %sender%\nFalscher Einladungscode!\n\n%sender%\n%signature%",
									"email:success" => "Glückwunsch!\n\nDu bist jetzt unter %mainaddress% zu erreichen. Die E-Mails werden an Deine Adresse %personaladdress% weitergeleitet.\nZusätzlich bist Du noch unter %aliasaddresses% erreichbar. Falls dort keine Adresse steht, wurde noch kein Kürzel für die Schule angegeben.\n\nWenn Du auch unter dieser Adresse E-Mails verfassen möchtest, kannst Du das unter %maillink% tun.\nDafür musst Du nur unter %passwordreseturl% einmalig ein Passwort setzen.\n\n%sender%\n%signature%",
									"email:handover" => "Die Weiterleitung für %mainaddress% sowie das Passwort für den Cloud-Zugang wurden geändert.\n\nDanke für Alles!\n\n%sender%\n%signature%",
									"email:signature" => "-------------------\nDiese E-Mail wurde automatisch aus dem XYZ-Portal versendet.\nBitte antworte nicht darauf. Wenn Du Probleme Hast, wende Dich bitte an die nächste Apotheke.",
									"email:sendername" => "",
									"email:senderaccount" => "",
									);
	foreach($defaults as $defkey => $defval) {
		$cmd="";
		if (!isset($dconfig[$defkey])) {
			$cmd="INSERT INTO sl_config (config_key,config_value) VALUES('".SQLite3::escapeString($defkey)."','".SQLite3::escapeString($defval)."');";
		} else {
			if ($force) {
				$cmd="UPDATE sl_config set config_value='".SQLite3::escapeString($defval)."' where config_key='".SQLite3::escapeString($defkey)."');";
			}
		}
		if ($cmd!="") {
			$res=db_exec($db,$cmd);
			if (!$res) { errormessage("unable to set config default; field:".$defkey,$db->lastErrorMsg()); die(); }
		}
	}
}

function db_throwtolog($level,$message) {
	$sql="insert into sx_log (log_datetime,log_severity,log_pid,log_text) VALUES (".microtime(true).",".($level*1).",".getmypid().",'".SQLite3::escapeString($message)."');";
	$dbLOG=db_getlink("LOG");
	$res=db_exec($dbLOG,$sql);
	if (!$res) { echo "error! ($sql) ".$dbLOG->lastErrorMsg()."\n"; }
}

function db_getlog_html() {
	$dbLOG=db_getlink("LOG");
	$entries=db_get_multi_entryC($dbLOG,"sx_log","where log_datetime > 0 ORDER BY log_datetime DESC");
	$out=""; $styletr="";
	$alter=""; $style="style=\"padding:2px; padding-left:5px; padding-right:5px; vertical-align:top\"";
	foreach($entries as $eid => $edata) {
		$ms=substr(floor(($edata["log_datetime"]-floor($edata["log_datetime"]))*1000)/1000,1);
		$logtext=htmlentities($edata["log_text"]);
		$logtext=str_replace("\n","<br>",$logtext);
		$logtext=str_replace(" ","&nbsp;",$logtext);
		$out.="<tr $alter $styletr><td $style>".str_replace(" ","&nbsp;",date("Y-m-d H:i:s",$edata["log_datetime"]).$ms)."</td><td $style>".$edata["log_pid"]."</td><td $style>".$edata["log_severity"]."</td><td $style>".$logtext."</td></tr>";
		if ($alter=="") { $alter="style=\"background-color:#e0e0e0\""; } else { $alter=""; }
	}
	return "<table>".$out."</table>";
}

function db_table_exists($db,$tablename) {
	$exists=db_query($db,"SELECT name FROM sqlite_master WHERE type='table' AND name='".$tablename."';");
	//debuglog("checking table existence ".$tablename." => ".print_r($exists,true));
	if (is_array($exists)) {
		return (count($exists)>0);
	} else {
		return ($exists!==false);
	}
}

function db_query($db,$query) {
	$res = $db->query($query);
	$output=false;
	if (($res!==false)) {
		$output=Array();
		while ($row = $res->fetchArray()) {
			$output[]=$row;
		}
	}
	return $output;
}

function db_exec($db,$query) {
	$res = $db->exec($query); 
	return $res;
}


// ============================================================================
//
// Mail-in-a-box functions
//
// ============================================================================



function mib_sendcommand($path,$method="GET",$items=array()) {
	$username=getconfig("mib_admin_username");
	$password=getconfig("mib_admin_password");
	$hostname=getconfig("mailhost");
	$itemstring="";
	foreach($items as $item => $itemvalue) {
		$itemstring.="-d \"".$item."=".addslashes($itemvalue)."\" ";
	}
	$curlcmd="-X $method \"https://".$hostname.$path."\" -u \"".$username.":".$password."\" ".$itemstring;
	//echo "sendcommand: $curlcmd\n";
	//$r=exec($curlcmd,$output,$result);
	//echo "sendcommand: $result $itemstring ".print_r($output,true)."\n";
	$citr=curlit($curlcmd);
	if (isset($items["password"])) { $items["password"]="<redacted>"; }
	if ($citr["headers"]["http_code"]!="200") {
		debuglog("error sending ".$path." ".$method." ".anytext($items)."\nresult:".anytext($citr),0);
	} else {
		debuglog("sending ".$path." ".$method." ".anytext($items)."\nresult:".anytext($citr),6);
	}
	return $citr;
}


// ============================================================================
//
// RoundCube functions
//
// ============================================================================



function rc_simulate_login($username,$password) {
	$baseurl="https://".getconfig("mailhost")."/mail/";
	$plcurl=curlit("-X GET ".$baseurl);
	debuglog("curl #1:".anytext($plcurl),8);
	$preloginpage=$plcurl["body"]; 
	$pcre="rcmail\.set_env\(({.*})\);"; $matches=array();
	preg_match("/".$pcre."/m",$preloginpage,$matches);
	if (count($matches)!=2) {
		errormessage("failed to parse set_env from prelogin page","");
	}
	$penv=json_decode($matches[1],true);
	if (!isset($penv["request_token"])) {
		errormessage("failed to parse request_token from prelogin page","");
	} 
	if (!isset($plcurl["cookies"]["roundcube_sessid"])) {
		errormessage("failed to parse cookie from prelogin page","");
	}

	$token=$penv["request_token"];
	$formdata=array("_token" => $token,"_task" => "login","_action" => "login","_url" => "_task=login","_user" => $username,"_pass" => $password);
	$itemstring="";
	foreach($formdata as $item => $itemvalue) {
		$itemstring.="-d \"".$item."=".addslashes($itemvalue)."\" ";
	}
	$curlcmd="-X POST \"".$baseurl."?_task=login\" -H \"Cookie: roundcube_sessid=".$plcurl["cookies"]["roundcube_sessid"]."\" ".$itemstring;
//	$r=exec($curlcmd,$output,$result);
	$licurl=curlit($curlcmd);
	debuglog("curl #2:".anytext($licurl),8);
	if (isset($licurl["cookies"]["roundcube_sessauth"]) && ($licurl["cookies"]["roundcube_sessauth"]!=="-del-")) {
		debuglog($username.": roundcube login simulation successful ");
		return true;
	}
	errormessage("roundcube login simulation failed ".$username." licurl:".anytext($licurl));
	return false;
}


// ============================================================================
//
// NextCloud functions
//
// ============================================================================


function nc_simulate_login($username,$password) {
	$localdebug=false;
	$baseurl="https://".getconfig("mailhost")."/cloud/index.php/login";
	$plcurl=curlit("-X GET ".$baseurl);
	debuglog("curl #1:".anytext($plcurl),8); 
	$preloginpage=$plcurl["body"]; 
	$pcre='data-requesttoken=\"(.*)\"'; $matches=array();
	preg_match("/".$pcre."/m",$preloginpage,$matches);
	if (count($matches)!=2) {
		errormessage("failed to parse requesttoken from prelogin page","");
	}
	if (!isset($plcurl["cookies"]["oc_sessionPassphrase"])) {
		errormessage("failed to parse cookie from prelogin page","");
	}
	$token=$matches[1];
	$cookies=array(); foreach($plcurl["cookies"] as $cookiename => $cookieval) { $cookies[]=$cookiename."=".$cookieval; } 
	$cookielist=join(";",$cookies);
	$formdata=array("user" => $username, "password" => $password, "requesttoken" => $token);
	$itemstring="";
	foreach($formdata as $item => $itemvalue) {
		$itemstring.="-d \"".$item."=".addslashes($itemvalue)."\" ";
	}
	$curlcmd="-X POST \"".$baseurl."\" -H \"Cookie: ".$cookielist."\" ".$itemstring;
//	$r=exec($curlcmd,$output,$result);
	$licurl=curlit($curlcmd);
	debuglog("curl #2:".anytext($licurl),8); 
	/*if (isset($licurl["cookies"]["roundcube_sessauth"]) && ($licurl["cookies"]["roundcube_sessauth"]!=="-del-")) {
		debuglog("nextcloud login simulation successful for $username");
		return true;
	}
	return false; */
	debuglog($username.": nextcloud login simulation successful");
	return true;
}

// ============================================================================
//
// ** db independent global functions
//
// ============================================================================


function random_str(
    $length,
    $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
) {
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    if ($max < 1) {
        throw new Exception('$keyspace must be at least two characters long');
    }
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[random_int(0, $max)];
    }
    return $str;
}

function randompassword() {
	return(sha1("mib:supersafe:".random_bytes(32)));
}

function curlit($curloptions) {
	$curlcmd="curl --include ".$curloptions;
	debuglog("command: $curlcmd",9);
	$r=exec($curlcmd. " 2>/dev/null",$output,$result);
	$curlresp=explode("\n\n",join("\n",$output),2);
	$out["curl_returncode"]=$result;
	if (count($curlresp)>0) {
		$out["headers_full"]=$curlresp[0];
		$headersx=explode("\n",$curlresp[0]);
		$httpheader=explode(" ",$headersx[0],3);
		$out["headers"]["http_code"]=$httpheader[1];
		if (count($httpheader)>2) {
			$out["headers"]["http_code_text"]=$httpheader[2];
		} else {
			$out["headers"]["http_code_text"]="";
		}
		for ($i=1;$i < count($headersx)-1;$i++) {
			$hdrline=explode(":",$headersx[$i],2);
			$headername=strtolower($hdrline[0]);
			$headervalue=trim(chop($hdrline[1]));
			if (isset($out["headers"][$headername])) {
				if (is_array($out["headers"][$headername])) {
					$out["headers"][$headername][]=$headervalue;
				} else {
					$out["headers"][$headername]=array($out["headers"][$headername],$headervalue);
				}
			} else {
				$out["headers"][$headername]=$headervalue;
			}
			if ($headername=="set-cookie") {
				$cookiex=explode(";",$headervalue);
				list($cookiename,$cookievalue)=explode("=",$cookiex[0],2);
				$out["cookies"][$cookiename]=$cookievalue;
			}
		}
		
	}
	if (count($curlresp)>1) {
		$out["body"]=$curlresp[1];
	}
	debuglog("result: ".anytext($out),9);
	return $out;
}

function runfromcli() {
	return (php_sapi_name()=="cli");
}

function stdlog($message) {
	echo $message."\n";
}

function debuglog($message,$level=1) {
	if ($level > getconfig("debuglevel")*1) { return; }
//	$cfunc=debug_backtrace(!DEBUG_BACKTRACE_PROVIDE_OBJECT|DEBUG_BACKTRACE_IGNORE_ARGS,2)[1]['function'];
	$cft=debug_backtrace(!DEBUG_BACKTRACE_PROVIDE_OBJECT|DEBUG_BACKTRACE_IGNORE_ARGS,2);
	if (isset($cft[1])) { $cfunc=$cft[1]['function']; } else { $cfunc="main"; }
	if (runfromcli()) {
		echo "> [".$cfunc."] ".$message."\n";
	}
	db_throwtolog($level,"[".$cfunc."] ".$message."\n");
}

function errormessage($message,$details="") {
	$cft=debug_backtrace(!DEBUG_BACKTRACE_PROVIDE_OBJECT|DEBUG_BACKTRACE_IGNORE_ARGS,2);
	if (isset($cft[1])) { $cfunc=$cft[1]['function']; } else { $cfunc="main"; }
	if (runfromcli()) {
		echo "! [".$cfunc."] ".$message."\n".$details;
	} else {
		//echo "<h1>".$message."</h1>".$details;
	}
	db_throwtolog(0,"[".$cfunc."] ".$message."\n".$details);
}

function xlog($message,$prefix="") {
	$cft=debug_backtrace(!DEBUG_BACKTRACE_PROVIDE_OBJECT|DEBUG_BACKTRACE_IGNORE_ARGS,2);
	if (isset($cft[1])) { $cfunc=$cft[1]['function']; } else { $cfunc="main"; }
	if (runfromcli()) {
		if ($prefix!="") { $prefix.=" "; }
		echo $prefix.$message."\n";
	}
	db_throwtolog(1,"[".$cfunc."] ".$message);
}





function anytext($any) {
	if ($any===null) { return "[null]"; }
	if ($any===true) { return "[true]"; }
	if ($any===false) { return "[false]"; }
	if (is_array($any)) { return print_r($any,true); }
	return $any;
}


function getvar($varname) {
	$data=array_merge($_GET,$_POST);
	if (!isset($data[$varname])) { return null; }
	return $data[$varname];
	
}

// ============================================================================
//
// webinterface
//
// ============================================================================

function replacei8n($replacestring,$varlist=array()) {
	$replacevars=array(
										 "unitdescription"=>db_get_config(null,"general:ou_description"));
	$i8nvars=geti8nvars("DE");
	$replacevars=array_merge($replacevars,$varlist,$i8nvars);
	$cx=$replacestring;
	do {
		$replaced=0;
		foreach($replacevars as $rvkey => $rvval) {
			$ocx=$cx; $cx=str_replace("#:inputvalue:".$rvkey."#","value=\"".addslashes($rvval)."\"",$cx); if ($ocx!=$cx) { $replaced++; }
			$ocx=$cx; $cx=str_replace("#:raw:".$rvkey."#",$rvval,$cx); if ($ocx!=$cx) { $replaced++; }
			$ocx=$cx; $cx=str_replace("#:html:".$rvkey."#",str_replace("\n","<p>",htmlentities($rvval)),$cx); if ($ocx!=$cx) { $replaced++; }
		}
	} while ($replaced > 0);
	return $cx;
}


function webif_page($header,$main) {
	$stylesheet=getstylesheet();
	$basetext=<<<EOM
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>#:html:_productname# - $header</title>

        <meta name="robots" content="noindex, nofollow">

$stylesheet
	</head>
	<body>
	$main
	</body>
</html>
EOM;

echo replacei8n($basetext);
	
	
}

function webif_single($header,$text) {
	if (!isajax()) {
		webif_page($header,"<section class=msgbox><h1>$header</h1><h3>$text</h3></section>");
	} else {
		respondajax(replacei8n($header.": ".$text),true,null);
	}
}

function webif_pwreset($passwordtoken) {
	$header="#:html:password reset#";
	$schulboxdomain=getconfig("maildomain");
	$text=<<<EOM

	<section class=msgbox><h1>$header</h1><div id=stepinfo>
	</div>
	<br>
	<div id="continue" class="clickitem clickitemlarge" onclick="nextstep()">#:html:continue#
	<div id="loadindicator" class="loadindi"><div></div><div></div><div></div><div></div></div>
	</div>
	</section>
	<script> 
	loadindicator(false);
	var currentstep=0;
	var token="$passwordtoken";
	var errormsg="";
	var mibemail="";
	function nextstep() {
		var stepinfo=document.getElementById("stepinfo");
		if (errormsg!="") { 
			errorhtml="<code>"+errormsg+"</code><br>";
			errormsg="";
		} else {
			errorhtml="";
		}
		newstep=currentstep; restep=false;
		if (currentstep==5) {
			stepinfo.innerHTML=errorhtml+"<h3>#:html:Bye!#</h3>";
			document.getElementById("continue").innerHTML="";
		}
		if (currentstep==4) {
			stepinfo.innerHTML=errorhtml+"<h3>#:html:Password successfully changed#</h3>#:html:you may now close this window#<br>";
			newstep=currentstep+1;
		}
		if (currentstep==3) {
			password1=document.getElementById("password1").value;
			password2=document.getElementById("password2").value;
			if (password1==password2) {
				mdata={ "mibemail": mibemail, "step": currentstep, "token": token,"password": password1 }
				rcall("pwreset",mdata,nextstep_cb);
			} else {
				errormsg="#:raw:passwords do not match#";
				newstep=2; restep=true;
			}
		}
		if (currentstep==2) {
			stepinfo.innerHTML=errorhtml+"<h3>#:html:Please enter your new password#</h3><input style=\"width:300px; color:#000000\" type=password autocomplete=off minlength=8 id=password1><br>#:html:please repeat your password#<br><input style=\"width:300px; color:#000000\" type=password autocomplete=off id=password2><br>";
			newstep=currentstep+1;
		}
		if (currentstep==1) {
			mibemail=document.getElementById("mibemail").value;
			mdata={ "mibemail": mibemail, "step": currentstep, "token": token }
			rcall("pwreset",mdata,nextstep_cb);
		}
		if (currentstep==0) {
			mibemail="";
			stepinfo.innerHTML=errorhtml+"<h3>#:html:Please enter your schulbox email address#</h3><input style=\"width:300px; color:#000000\" type=text id=mibemail placeholder=\"#:html:e.g.# my-schulbox-email@$schulboxdomain\">";
			newstep=currentstep+1;
		}
		console.log("nextstep currentstep",currentstep,"errormsg",errormsg,"newstep",newstep,"restep",restep)
		currentstep=newstep;
		if (restep) { nextstep(); }

	}
	
	function nextstep_cb(rdata) {
		//if (currentstep==1) {
			if (rdata.jsond.error==false) {
				currentstep++;
			} else {
				currentstep--;
				errormsg=rdata.jsond.message;
			}
			nextstep();
		//}
	}
	
	nextstep();
	</script>
EOM;
	webif_page($header,$text);
}


function webif_main($username) {
	$contentvars=webif_contents(getvar("function"));
	$cx=webif_base(array());
	
	$cx=replacei8n($cx,$contentvars);
	echo $cx;


}

function invitecode_clean($icode) {
	$icode=str_replace(" ","",$icode);
	$icode=str_replace("-","",$icode);
	$icode=str_replace("/","",$icode);
	$icode=str_replace(".","",$icode);
	$icode=chop($icode);
	return $icode;
}

function invitecode_format($icode) {
	$icode=invitecode_clean($icode);
	if (strlen($icode)!=12) { return $icode; }
	return (substr($icode,0,4)."-".substr($icode,4,4)."-".substr($icode,8,4));
}

function sanitizesql($sqlstr) {
	
}

function ajax_main($cookieuser) {
	$data = json_decode(file_get_contents('php://input'), true);
	if (isset($data["what"])) { $what=$data["what"]; } else { $what=""; }
	if ($what=="table_getdata") {
		$rdata=array();
		if ($data["mdata"]["table"]=="orgunit") {
			$db=db_getlink("self");
			$tablename="sl_orgunit";
			$countcol="ou_id";
			$tprefix="ou_";
		}
		if ($data["mdata"]["table"]=="log") {
			$db=db_getlink("LOG");
			$tablename="sx_log";
			$countcol="log_id";
			$tprefix="log_";
		}

		$order=""; $limit=""; $where="";
		//$where="where hash='".SQLite3::escapeString($token)."'";
		if (isset($data["mdata"]["tableconfig"])) {
			foreach($data["mdata"]["tableconfig"] as $colname => $colconfig) {
				if (isset($colconfig["sort"]) && ($colconfig["sort"]!==false)) { $order=" ORDER BY \"".$tprefix.SQLite3::escapeString($colname)."\" ".SQLite3::escapeString($colconfig["sort"]); }
				if (isset($colconfig["filter"]) && ($colconfig["filter"]!==false) && isset($colconfig["filter"]["value"]) && isset($colconfig["filter"]["operator"])) { 
					$val=$colconfig["filter"]["value"];
					$cond=""; $usecol=$colname;
					if (isset($data["mdata"]["coldata"][$colname]) && (isset($data["mdata"]["coldata"][$colname]["subdata"]))) {
						list($usecol,$usesub)=explode(":",$colname,2);
						$val="*\"".$usesub."\":\"*".str_replace("*","",$val)."*\"*";
					}

					if (($colconfig["filter"]["operator"]=="=") || ($colconfig["filter"]["operator"]=="!=")) { 
						$op="="; 
						$val=str_replace("*","%",$val);
						if (strpos($val,"%")!==false) { $op="LIKE"; }
						if ($colconfig["filter"]["operator"]=="!=") { $op="NOT ".$op; }
						$cond.=$op." '".SQLite3::escapeString($val)."'";
					}
					if (($colconfig["filter"]["operator"]==">") || ($colconfig["filter"]["operator"]=="<")) {
						$cond.=$colconfig["filter"]["operator"]." '".SQLite3::escapeString($val)."'";
					}
					if ($cond!="") {
						if ($where !="") { $where.=" AND "; } else { $where="WHERE "; }
						$where.="(".$tprefix.SQLite3::escapeString($usecol)." ".$cond.")";
					}
				}
			}
		}
		if (isset($data["mdata"]["maxmatches"])) {
			$limit=" LIMIT ".SQLite3::escapeString($data["mdata"]["maxmatches"]);
			$rdata["dbrequest"]["limit"]=$data["mdata"]["maxmatches"];
			if (isset($data["mdata"]["startindex"])) {
				$limit.=" OFFSET ".SQLite3::escapeString($data["mdata"]["startindex"]);
				$rdata["dbrequest"]["offset"]=$data["mdata"]["startindex"];
			}
		}
		
		$sql="select count(".$countcol.") from ".$tablename." ".$where;
		$dbrsp=db_query($db,$sql);
		$rdata["dbrequest"]["totalmatches"]=$dbrsp[0][0];
		$sqlwhere=$where.$order.$limit;
		$rdata["dbrequest"]["sql"]=$sqlwhere;
		$rsp=db_get_multi_entryC($db,$tablename,$sqlwhere);
		if ($rsp!==null) {
			foreach($rsp as $ouid => $oudata) {
				foreach($oudata as $oudid => $ouddata) {
					if ((substr($oudid,0,3)!="ou_") && (substr($oudid,0,4)!="log_")) {
						unset($rsp[$ouid][$oudid]);
					}
				}
				if ($data["mdata"]["table"]=="orgunit") {
					$rsp[$ouid]["ou_invitecode_isset"]=($rsp[$ouid]["ou_invitecode"]!="");
					$rsp[$ouid]["ou_invitecode"]=invitecode_format($rsp[$ouid]["ou_invitecode"]);
					$rsp[$ouid]["ou_actions"]=array("passwordresetlink" => array("mib_email" => $rsp[$ouid]["ou_mib_email"]));
					if ((db_get_config($db,"general:use_abbreviationfield")*1) > 0) {
						$rsp[$ouid]["ou_actions"]["changeabbreviation"]=array("mib_email" => $rsp[$ouid]["ou_mib_email"],"currentabbrev" => $rsp[$ouid]["ou_abbreviation"]);
					}
					$rsp[$ouid]["ou_actions"]["newinvitecode"] = array("mib_email" => $rsp[$ouid]["ou_mib_email"]);
					$rsp[$ouid]["ou_actions"]["removeinvitecode"] = array("mib_email" => $rsp[$ouid]["ou_mib_email"]);
					
					
				}
			}
		}
		
		$rdata=array_merge($rdata,array($data["mdata"]["table"] => $rsp));
		respondajax("OK",false,$rdata); die();
	}
	if ($what=="pwresetlink") {
		if (isset($data["mdata"]["mib_email"])) {
			$db=db_getlink("self");
			$where="where ou_mib_email='".SQLite3::escapeString($data["mdata"]["mib_email"])."';";
			$usresult=db_get_single_entryC($db,"sl_orgunit",$where);
			//$rdata["debug"]=$usresult;
			if ($usresult===null) {
				respondajax("Error: no org found for mib_email ".$data["mdata"]["mib_email"],true,$rdata); die();
			}
			if (!isset($usresult["ou_personal_email"]) || ($usresult["ou_personal_email"]=="")) {
				respondajax("Error: no personal email address configured ",true,$rdata); die();
			}
			//$rdata["debug"]=$usresult;

			$mailvars=array();
			$pwresetlink=generate_passwordresetlink($db,$data["mdata"]["mib_email"]);
			$mailvars["passwordreseturl"]=$pwresetlink;
			$mailvars["mainaddress"]=$data["mdata"]["mib_email"];
			$mailvars["personname"]=$usresult["ou_personal_name"];
			mail_template("pwresetlink", array("to" => $usresult["ou_personal_email"],"vars" => $mailvars));
			
			
			respondajax("OK",false,$rdata); die();
		} else {
			respondajax("Error!",true,null); die();
		}
	}
	if ($what=="pwreset") {
		$rdata=array();
		if ((!isset($data["mdata"]["token"])) || (!isset($data["mdata"]["mibemail"])) || (!isset($data["mdata"]["step"])) ) {
			respondajax("#:raw:Error#: #:raw:invalid function call#",true,$rdata); die();
		}
		$db=db_getlink("self");
		$pwtokstrg=db_get_single_entry($db,"storage","passwordtoken:".$data["mdata"]["token"]);
		if ($pwtokstrg===null) {
			respondajax("#:raw:Error#: #:raw:invalid token# ",true,$rdata); die();
		}
		$pwtokdata=unserialize($pwtokstrg);
		if (!isset($pwtokdata["userid"])) {
			respondajax("#:raw:Error#: #:raw:invalid token# ",true,$rdata); die();
		}
		if (strtolower($pwtokdata["userid"])!=strtolower($data["mdata"]["mibemail"])) {
			respondajax("#:raw:Error#: #:raw:invalid mibemail# ",true,$rdata); die();
		}
		$okmessage="";
		if ($data["mdata"]["step"]=="1") { $okmessage="#:raw:valid user#"; }
		if ($data["mdata"]["step"]=="3") { 
			if ((!isset($data["mdata"]["password"])) || ($data["mdata"]["password"]=="")) {
				respondajax("#:raw:Error#: #:raw:missing password#",true,$rdata); die();
			}
			mib_sendcommand("/admin/mail/users/password","POST",array("email" => $data["mdata"]["mibemail"],"password" => $data["mdata"]["password"]));
			$pwtokdata["used"]=array("userid"=>$pwtokdata["userid"],"date" => time());
			unset($pwtokdata["userid"]);
			$pwtokstrg=serialize($pwtokdata);
			db_set_single_entry($db,"storage","passwordtoken:".$data["mdata"]["token"],$pwtokstrg);
			$okmessage="#:raw:password changed#"; 
			$where="where ou_mib_email='".SQLite3::escapeString($data["mdata"]["mibemail"])."';";
			$usresult=db_get_single_entryC($db,"sl_orgunit",$where);
			$mailvars["mainaddress"]=$data["mdata"]["mibemail"];
			$mailvars["personname"]=$usresult["ou_personal_name"];
			mail_template("pwreset", array("to" => $usresult["ou_personal_email"],"vars" => $mailvars));
		}
		if ($okmessage!="") {
			respondajax($okmessage,false,$rdata); die();
		} else {
			respondajax("#:raw:Error#: #:raw:invalid function call#",false,$rdata); die();
		}
		
	}
	if ($what=="chgabbrev") {
		$rdata=array();
		$db=db_getlink("self");
		if ((!isset($data["mdata"]["mib_email"])) || (!isset($data["mdata"]["newabbrev"]))) {
			respondajax("#:raw:Error#: #:raw:invalid function call#",true,$rdata); die();
		}
		$where="where ou_mib_email='".SQLite3::escapeString($data["mdata"]["mib_email"])."';";
		$ouresult=db_get_single_entryC($db,"sl_orgunit",$where);
		if ($ouresult===null) {
			respondajax("#:raw:Error#: no org found for mib_email ".$data["mdata"]["mib_email"],true,$rdata); die();
		}
		$mainaddress=$data["mdata"]["mib_email"];
		$oldabbrev=substr(getslugpart($ouresult["ou_abbreviation"]),0,db_get_config($db,"general:use_abbreviationfield"));
		$oldlocal=$oldabbrev;
		$newabbrev=substr(getslugpart($data["mdata"]["newabbrev"]),0,db_get_config($db,"general:use_abbreviationfield"));
		$newlocal=$newabbrev;
		if (getconfig("slug_include_city")===true) { 
			$newlocal.=".".getslugpart($ouresult["ou_address_city"],"_",false);
			$oldlocal.=".".getslugpart($ouresult["ou_address_city"],"_",false);
		}
		$maildomain=getconfig("maildomain");
		// remove old mail alias
		if ($ouresult["ou_abbreviation"]!="") {
			debuglog($mainaddress.": removing mail alias: ".$oldlocal,2);
			$rc=mib_sendcommand("/admin/mail/aliases/remove","POST",array("address" => $oldlocal."@".$maildomain));
			if ($rc["headers"]["http_code"]!=="200") {
				respondajax("#:raw:Error#: removal of mail alias failed ".$rc["body"],true,$rdata); die();
			}
		}
		// add new mail alias
		if ($data["mdata"]["newabbrev"]!="") {
			debuglog($mainaddress.": adding mail alias: ".$newlocal,2);
			$rc=mib_sendcommand("/admin/mail/aliases/add","POST",array("update_if_exists" => 0,"address" => $newlocal."@".$maildomain, "forwards_to" => $mainaddress,"permitted_senders" => ""));
			if ($rc["headers"]["http_code"]!=="200") {
				respondajax("#:raw:Error#: addition of mail alias failed ".$rc["body"],true,$rdata); die();
			}
		}
		// update DB
		$cmd="update sl_orgunit set ou_abbreviation='".SQLite3::escapeString(strtoupper($newabbrev))."' where ou_id=".($ouresult["ou_id"]*1).";";
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage($mainaddress.": unable to update user in DB ($cmd) ",$db->lastErrorMsg()); 
			respondajax("Error: DB update failed ".$db->lastErrorMsg(),true,$rdata); die();
		}
		respondajax("#:raw:changed abbreviation to# '".strtoupper($newabbrev)."'",false,$rdata); die();
	}
	if ($what=="chginvite") {
		$rdata=array();
		$db=db_getlink("self");
		if ((!isset($data["mdata"]["mib_email"])) || (!isset($data["mdata"]["removeinvite"]))) {
			respondajax("#:raw:Error#: #:raw:invalid function call#",true,$rdata); die();
		}
		$where="where ou_mib_email='".SQLite3::escapeString($data["mdata"]["mib_email"])."';";
		$ouresult=db_get_single_entryC($db,"sl_orgunit",$where);
		if ($ouresult===null) {
			respondajax("#:raw:Error#: no org found for mib_email ".$data["mdata"]["mib_email"],true,$rdata); die();
		}
		if ($data["mdata"]["removeinvite"]==true) {
			$okmessage="#:raw:invite code removed#";
			$newinvite="";
		} else {
			$newinvite=db_generate_invitecode($db);
			$okmessage="#:raw:new invite code generated#";
		}
		// update DB
		$cmd="update sl_orgunit set ou_invitecode='".SQLite3::escapeString($newinvite)."' where ou_id=".($ouresult["ou_id"]*1).";";
		$res=db_exec($db,$cmd);
		if (!$res) { errormessage($mainaddress.": unable to update user in DB ($cmd) ",$db->lastErrorMsg()); 
			respondajax("#:raw:Error#: DB update failed ".$db->lastErrorMsg(),true,$rdata); die();
		}
		respondajax($okmessage,false,$rdata); die();
	}
	
	respondajax("#:raw:Error#: #:raw:unknown function#: ".$what,true,null); die();
}


function isJson($string) {
   json_decode($string);
   return json_last_error() === JSON_ERROR_NONE;
}


function webif_showtable($dbtable,$cols,$tableconfig,$ignoreurltblconfig=false,$callback=null) {
		if ($dbtable=="orgunit") { $tprefix="ou_"; }
		if ($dbtable=="log") { $tprefix="log_"; }
		if ((getvar("table")!==false) && (!$ignoreurltblconfig)) {
			$tblc=getvar("table");
			if (isJson($tblc)) {
				$tableconfig=json_decode($tblc,true);
			}
		}
		$theads="<thead><tr>";
		foreach ($cols as $colid => $coldata) {
			$theads.="<th class=\"thbase col_".$colid."\" id=\"th_".$colid."\" style=\"vertical-align: top;\"><div class=\"thcontainer\">".$coldata["desc"]."<div class=\"thcontrols\" id=\"thc_".$colid."\"><ul class=\"nav tblnav\">";
			//if (!isset($coldata["subdata"]) || ($coldata["subdata"]===false)) { $theads.="<a id=\"".$colid."_sort_ASC\" onclick=\"changecol('".$colid."','sort','ASC')\">^</a> <a id=\"".$colid."_sort_DESC\" onclick=\"changecol('".$colid."','sort','DESC')\">v</a>"; }
			//if (!isset($coldata["subdata"]) || ($coldata["subdata"]===false)) { $theads.="<li class=\"clickitem clickitemsmall thcontrol caretup\" id=\"".$colid."_sort_ASC\" onclick=\"changecol('".$colid."','sort','ASC')\"></li> <li class=\"clickitem clickitemsmall thcontrol caret\" id=\"".$colid."_sort_DESC\" onclick=\"changecol('".$colid."','sort','DESC')\"></li>"; }
			if (isset($coldata["type"]) && ($coldata["type"]!="static")) {
				$theads.="<li class=\"clickitem thcontrol dropdown\" data-toggle=\"dropdown\" id=\"".$colid."_filter\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\"><b class=\"caret\"></b></a>";
				$theads.="<ul id=\"".$colid."_filter_dropdown\" class=\"dropdown-menu\"></ul>";
				$theads.="</li>";
			}
			$theads.="</ul></div></div></th>";
		}
		if ($callback!==null) { $theads.call_user_func($callback,null,"header"); }
		$theads.="</tr></thead>";
		$col_json=json_encode($cols);
		$tableconfig_json=json_encode($tableconfig);
		$posturl=$_SERVER["PHP_SELF"]."?ajax=1";
		$pageurl=$_SERVER["REQUEST_URI"];
		$cb = function ($fn) { return $fn; };
		$svgsize=24;
		$tbuttons="";
		if ($callback!==null) { $tbuttons.call_user_func($callback,null,"buttons"); }
		$returnstr=<<<EOM
<script src="/admin/assets/jquery.min.js"></script>
<script src="/admin/assets/bootstrap/js/bootstrap.min.js"></script>
<style>



</style>
<div style="display:inline-block">
<div style="display:inline-block">
<div class="clickitem clickitemlarge" onclick="refresh()">{$cb(getsvg("refresh",$svgsize,"#:raw:refresh#"))}</div>
<div class="clickitem clickitemlarge" id="btn_autorefresh" onclick="auto_refresh()">{$cb(getsvg("refresh_timed",$svgsize,"#:raw:auto refresh#"))}</div>
<div class="clickitem clickitemlarge" id=directlinkdiv><a onclick="prevent" id=directlink title="#:raw:_directlinkinfo#" href=#>{$cb(getsvg("directlink",$svgsize,"#:raw:_directlinkinfo#"))}</a></div>
</div><br>
<div style="display:inline-block">
<div class="clickitem clickitemlarge" onclick="changecol('','clearsort','')">{$cb(getsvg("sort_off",$svgsize,"#:raw:clear sort#"))}</div>
<div class="clickitem clickitemlarge" onclick="changecol('','clearfilter','')">{$cb(getsvg("filter_off",$svgsize,"#:raw:clear filter#"))}</div>
<div class="clickitem clickitemlarge" onclick="changecol('','clearall','')">{$cb(getsvg("all_off",$svgsize,"#:raw:clear all#"))}</div>
</div><br><br>
<div style="display:inline-block"><div style="float:left">#:html:datasets# #:html:per page#: &nbsp; </div>
<div class="clickitem scopeselsize" id="scopesel_1" onclick="changescope(0,1)"> 1 </div>
<div class="clickitem scopeselsize" id="scopesel_20" onclick="changescope(0,20)">20 </div>
<div class="clickitem scopeselsize" id="scopesel_50" onclick="changescope(0,50)"> 50 </div>
<div class="clickitem scopeselsize" id="scopesel_100" onclick="changescope(0,100)"> 100 </div>
</div>
<br>
<div id="scopenavigation">
<div style="float:left;" id="scopenavigation_left">
<div class="clickitem scopeselx scopeselxl" id="scopesel_xleft" onclick="changescopex(-5)"> {$cb(getsvg("leftmost",$svgsize,"#:raw:first page#"))} </div>
<div class="clickitem scopeselx scopeselxl" id="scopesel_left"  onclick="changescopex(-1)"> {$cb(getsvg("left",$svgsize,"#:raw:previous page#"))} </div>
</div>
<div style="width:200px; float:left; background:lightgreen; text-align:center; padding:2px; margin:2px;" id="scopelocation"></div>
<div style="float:left;" id="scopenavigation_right">
<div class="clickitem scopeselx scopeselxr" id="scopesel_right"  onclick="changescopex(1)"> {$cb(getsvg("right",$svgsize,"#:raw:next page#"))} </div>
<div class="clickitem scopeselx scopeselxr" id="scopesel_xright" onclick="changescopex(5)"> {$cb(getsvg("rightmost",$svgsize,"#:raw:last page#"))} </div>
</div>
</div>
<table class=table style="width:auto">
$theads
<tbody id="tabledatabody"></tbody>
</table>
<script>
var scopesize=20
var scopestart=0
var currentpage=0
var totalpages=0
var autorefresh=false
var autorefreshobj=null
var coldata=JSON.parse('$col_json');
var tableconfig=JSON.parse('$tableconfig_json');
console.log("coldata: ",coldata);

/**
 * Convert a string to HTML entities
 */
String.prototype.toHtmlEntities = function() {
    return this.replace(/./gm, function(s) {
        // return "&#" + s.charCodeAt(0) + ";";
        return (s.match(/[a-z0-9\s]+/i)) ? s : "&#" + s.charCodeAt(0) + ";";
    });
};

/**
 * Create string from HTML entities
 */
String.fromHtmlEntities = function(string) {
    return (string+"").replace(/&#\d+;/gm,function(s) {
        return String.fromCharCode(s.match(/\d+/gm)[0]);
    })
};


function prevent(e) {
	console.log("e",e);
	e.preventDefault();
	e.stopPropagation();
	return null;
}

function update_filter(colname,filvalue,filoperator) {
	if (notnull(coldata[colname])) {
		if (filvalue===false) {
			if (!notnull(tableconfig[colname])) { tableconfig[colname]={}; }
			tableconfig[colname]["filter"]=false;
			update_table_config(); refresh_table_data();
			return true;
		}
		if (filvalue===null) {
			filvalue=document.getElementById(colname+"_filter_input").value;
			if (!notnull(tableconfig[colname])) { tableconfig[colname]={}; }
			tableconfig[colname]["filter"]={};
			tableconfig[colname]["filter"]["value"]=filvalue;
			tableconfig[colname]["filter"]["operator"]=filoperator;
			update_table_config(); refresh_table_data();
		}
	}
}

function filteritem(colkey,coltype,filterop,currentfilter=false) {
	filterophtml=filterop;
	if ( filterop=="<" ) { filteroptxt="LT"; filterophtml="&lt;"; }
	if ( filterop==">" ) { filteroptxt="GT"; filterophtml="&gt;"; }
	if ( filterop=="=" ) { filteroptxt="EQ"; }
	if ( filterop=="!=" ) { filteroptxt="NEQ"; }
	var fux="";
	var validcol= ( 
									((coltype=="number") && ((filterop=="<") || (filterop==">") || (filterop=="=") || (filterop=="!="))) ||
									((coltype=="text") && ((filterop=="=") || (filterop=="!="))) 
								);
	var currentf=( notnull(currentfilter["operator"]) && (currentfilter["operator"]==filterop));
	if (validcol) {
		if (currentf) {
			fux=" mactive=\"1\"";
		} else {
			fux=" mactive=\"0\"";
		}
	}
	if (fux!="") {
		return "<div class=\"clickitem clickitemsmall\" id=\""+colkey+"_filter_op"+filteroptxt+"\" onclick=\"changecol('"+colkey+"','filter',null,'"+filterop+"')\""+fux+">"+filterophtml+"</div> &nbsp; ";
	} else {
		return "";
	}
}

function update_table_config() {
	for (const [tblkey, tblvalue] of Object.entries(tableconfig)) {
		if (tblvalue["show"]===false) {
			$('.col_'+tblkey).hide();
		} else {
			$('.col_'+tblkey).show();
		}
		console.log("update_table_config tbl ",tblkey,tblvalue);
	}
	for (const [colkey, colvalue] of Object.entries(coldata)) {
		if (notnull(colvalue["type"])) {
			var filterul=document.getElementById(colkey+"_filter_dropdown");
			if (filterul) {
				var fuhtml=""
				var fuiinput=""
				var havefilter=(notnull(tableconfig[colkey]) && notnull(tableconfig[colkey]["filter"]) && (tableconfig[colkey]["filter"]!==false))
				if (havefilter) { fuiinput=" value='"+tableconfig[colkey]["filter"]["value"]+"'"; } else { 
					if (!notnull(tableconfig[colkey])) { tableconfig[colkey]={}; }
					if (!notnull(tableconfig[colkey]["filter"])) { tableconfig[colkey]["filter"]=false; }
				}
				fuhtml+="<li class=\"dropdown-header\">#:html:Sort# "+colvalue["desc"]+"</li>";
				fuhtml+="<li class=\"clickitem clickitemlarge\" id=\""+colkey+"_sort_ASC\" onclick=\"changecol('"+colkey+"','sort','ASC')\">#:html:ascending#</li>";
				fuhtml+="<li class=\"clickitem clickitemlarge\" id=\""+colkey+"_sort_DESC\" onclick=\"changecol('"+colkey+"','sort','DESC')\">#:html:descending#</li>"; 
				fuhtml+="<li class=\"divider\"></li>";
				fuhtml+="<li class=\"dropdown-header\">#:html:Filter# "+colvalue["desc"]+"</li><li \"dropdown-header\">#:html:Value#:</li>";
				fuhtml+="<li><input type=text id=\""+colkey+"_filter_input\" "+fuiinput+"></li><li> ";
				fuhtml+=filteritem(colkey,colvalue["type"],"<",tableconfig[colkey]["filter"]) 
				fuhtml+=filteritem(colkey,colvalue["type"],"=",tableconfig[colkey]["filter"]);
				fuhtml+=filteritem(colkey,colvalue["type"],"!=",tableconfig[colkey]["filter"]);
				fuhtml+=filteritem(colkey,colvalue["type"],">",tableconfig[colkey]["filter"]) 
				/*
				if (colvalue["type"]=="number") { fuhtml+="<li><div class=\"clickitem clickitemsmall\" id=\""+colkey+"_filter_opLT\" onclick=\"update_filter('"+colkey+"',null,'<')\">&lt;</div> &nbsp;"; }
					fuhtml+="<div class=\"clickitem clickitemsmall\" id=\""+colkey+"_filter_opEQ\" onclick=\"update_filter('"+colkey+"',null,'=')\">=</div> &nbsp;";
					fuhtml+="<div class=\"clickitem clickitemsmall\" id=\""+colkey+"_filter_opNEQ\" onclick=\"update_filter('"+colkey+"',null,'!=')\">!=</div> &nbsp;";
				if (colvalue["type"]=="number") { fuhtml+="<div class=\"clickitem clickitemsmall\" id=\""+colkey+"_filter_opGT\" onclick=\"update_filter('"+colkey+"',null,'>')\">&gt;</div> &nbsp;"; } */
				fuhtml+="</li>"; 
				if (havefilter) {
					fuhtml+="<li class=\"divider\"></li><li><a onclick=\"changecol(\'"+colkey+"\','filter',false,false)\">#:html:remove filter#</a></li>";
				} 
				if (havefilter || (notnull(tableconfig[colkey]) && notnull(tableconfig[colkey]["sort"]) && (tableconfig[colkey]["sort"]!==false))) {
					document.getElementById(colkey+"_filter").setAttribute('mactive', '1');
				} else {
					document.getElementById(colkey+"_filter").setAttribute('mactive', '0');
				}
				filterul.innerHTML=fuhtml;
				var fulinput=document.getElementById(colkey+"_filter_input");
				if (fulinput) {
					fulinput.addEventListener('click', prevent)
				}
				update_indicators()
			}
		}
		console.log("update_table_config col ",colkey,colvalue);
	}
	scopestart=0
	currentpage=0
}

function changecol(colname,changewhat,changehow,changehow2=null) {
	console.log("changecol ",colname,changewhat,changehow,changehow2);

	for (const [tblkey, tbldata] of Object.entries(tableconfig)) {
		if ((changewhat=="sort") || (changewhat=="clearall") || (changewhat=="clearsort")) {
			tableconfig[tblkey]["sort"]=false;
		}
		if ((changewhat=="clearall") || (changewhat=="clearfilter")) {
			tableconfig[tblkey]["filter"]=false;
		}
	}
	if (changewhat=="sort") {
		if (!notnull(tableconfig[colname])) { tableconfig[colname]={}; }
		if (!notnull(tableconfig[changehow])) { tableconfig[colname]["sort"]=changehow; }
			
	}
	if (changewhat=="filter") {
		if (notnull(coldata[colname])) {
			if (changehow===false) {
				if (!notnull(tableconfig[colname])) { tableconfig[colname]={}; }
				tableconfig[colname]["filter"]=false;
			}
			if (changehow===null) {
				changehow=document.getElementById(colname+"_filter_input").value;
			if (!notnull(tableconfig[colname])) { tableconfig[colname]={}; }
					tableconfig[colname]["filter"]={};
				tableconfig[colname]["filter"]["value"]=changehow;
				tableconfig[colname]["filter"]["operator"]=changehow2;
			}
		}
	}
	
	document.getElementById("directlink").href="$pageurl"+"&table="+JSON.stringify(tableconfig);
	
	scopestart=0
	currentpage=0
	update_indicators();
	update_table_config();
	refresh_table_data();
}

function update_indicators() {
	for (const [colkey, colval] of Object.entries(coldata)) {
			if (document.getElementById(colkey+"_sort_ASC")) { 
				if (notnull(tableconfig[colkey]) && notnull(tableconfig[colkey]["sort"]) && (tableconfig[colkey]["sort"]=="ASC")) {
					document.getElementById(colkey+"_sort_ASC").setAttribute('mactive', '1');
				} else {
					document.getElementById(colkey+"_sort_ASC").setAttribute('mactive', '0');
				}
			}
			if (document.getElementById(colkey+"_sort_DESC")) { 
				if (notnull(tableconfig[colkey]) && notnull(tableconfig[colkey]["sort"]) && (tableconfig[colkey]["sort"]=="DESC")) {
					document.getElementById(colkey+"_sort_DESC").setAttribute('mactive', '1');
				} else {
					document.getElementById(colkey+"_sort_DESC").setAttribute('mactive', '0');
				}
			}
		}
	if (autorefresh!==false) {
		document.getElementById("btn_autorefresh").setAttribute('mactive', '1');
	} else {
		document.getElementById("btn_autorefresh").setAttribute('mactive', '0');
	}
}

function changescopex(changeval) {
	console.log("changescopex changeval:",changeval," currentpage",currentpage," totalpages",totalpages)
	if (changeval < 0) {
		if (currentpage==0) { return false; }
		if (changeval==-5) { changescope(0,null); return true; }
		if (changeval==-1) { changescope((currentpage-1)*scopesize,null); return true; }
	}
	if (changeval > 0) {
		if (currentpage==totalpages) { return false; }
		if (changeval==5) { changescope(totalpages*scopesize,null); return true; }
		if (changeval==1) { changescope((currentpage+1)*scopesize,null); return true; }
	}
}

function changescope(startindex,maxmatches) {
	console.log("changescope ",startindex,maxmatches);
	var ctlitems=document.getElementsByClassName("scopeselsize");
	Array.prototype.forEach.call(ctlitems, function(ctlitem) {
		ctlitem.setAttribute('mactive', '0');
	}); 
	//$("#scopeselsize").children().prop("mactive",0)
	if (notnull(maxmatches)) { 
		startindex=0;
		scopesize=maxmatches;
	}
	scopestart=startindex;
	ctlitem=document.getElementById("scopesel_"+scopesize);
	if (ctlitem) { 
		//ctlitem.style="background-color:#ffe0e0"; 
		ctlitem.setAttribute('mactive', '1');
	}
	refresh_table_data();
}

function update_table_data(jdata) {
	data=jdata.data["$dbtable"];
	tbody=document.getElementById("tabledatabody");
	tbody.innerHTML="";
	countentries=0
	if (data.length > 0) {
		alternate=true;
		// check for subdata filtering (prepare subdatafilter object)
		var subdatafiltering=false;
		var subdatafilter={};
		for (const [colkey, colvalue] of Object.entries(coldata)) {
			if (notnull(colvalue["subdata"]) && (colvalue["subdata"]===true)) {
				if (notnull(tableconfig[colkey]) && notnull(tableconfig[colkey]["filter"]) && (tableconfig[colkey]["filter"]!==false)) {
					subdatafiltering=true;
					if (!notnull(subdatafilter[colvalue["subdatadetails"]["master"]])) {
						subdatafilter[colvalue["subdatadetails"]["master"]]={};
					}
					if (!notnull(subdatafilter[colvalue["subdatadetails"]["master"]][colvalue["subdatadetails"]["field"]])) {
						subdatafilter[colvalue["subdatadetails"]["master"]][colvalue["subdatadetails"]["field"]]={};
					}
					subdatafilter[colvalue["subdatadetails"]["master"]][colvalue["subdatadetails"]["field"]]=tableconfig[colkey]["filter"];
				}
			}
		}
		
		data.forEach(function (dataitem, dataindex) {
			var content="";
			var wantentry=(!subdatafiltering);
			// do subdata filtering (cannot be done on SQL level)
			if (subdatafiltering) {
				//console.log("sdf coldata",coldata,"sdf tableconfig",tableconfig,"sdf dataitem",dataitem,"subdatafilter",subdatafilter);
				for (const [colkey, colvalue] of Object.entries(coldata)) {
					//console.log("sdf col",colkey,colvalue)
					if (notnull(colvalue["subdatadetails"]) && notnull(subdatafilter[colvalue["subdatadetails"]["master"]]) && (notnull(subdatafilter[colvalue["subdatadetails"]["master"]][colvalue["subdatadetails"]["field"]]))) {
						currentfilter=subdatafilter[colvalue["subdatadetails"]["master"]][colvalue["subdatadetails"]["field"]]
						currentvalue=dataitem["$tprefix"+colvalue["subdatadetails"]["master"]]
						if (isJsonString(currentvalue)) {
							currentvaljson=JSON.parse(currentvalue); rx="";
							if (notnull(currentvaljson[colvalue["subdatadetails"]["field"]])) {
								rx="(^"+currentfilter["value"].replaceAll("*","%").replaceAll("%",".*")+"$)"
								if (currentfilter["operator"]=="!=") { rx="!"+rx; }
								var regex=new RegExp(rx)
								wantentry=regex.test(currentvaljson[colvalue["subdatadetails"]["field"]])
							}
							console.log("sdf currentvalue:",currentvaljson,rx,wantentry);
						} else {
							// fallback
							wantentry=true;
						}
						//console.log("sdf currentfilter:",colkey,currentfilter,currentvalue,dataitem);
					}
				}
			}
			if (wantentry) {
				countentries++;
				for (const [key, value] of Object.entries(coldata)) {
					content+="<td>";
					if (!notnull(value["type"])) { value["type"]="text"; }
					contentvalue=""
					//console.log("TT "+key+" ==============")
					splitkey=key.split(":",2)
					//console.log("TT "+key+" splitkey",splitkey)
					if (splitkey.length>1) {
						valuesel=dataitem["$tprefix"+splitkey[0]];
						//console.log("TT "+key+" valuesel",valuesel)
						if (isJsonString(valuesel)) {
							subvalues=JSON.parse(valuesel);
							//console.log("TT "+key+" subvalues",subvalues)
							if (notnull(subvalues[splitkey[1]])) {
								//console.log("TT "+key+" subvalues1",subvalues[splitkey[1]])
								contentvalue=subvalues[splitkey[1]];
							}
						}
					} else {
						contentvalue=dataitem["$tprefix"+key]
					}
					//console.log("TT "+key+" >>",contentvalue);
					
					if (value["type"]=="static") { 
						if (typeof contentvalue === 'object' && !Array.isArray(contentvalue)) {
							// actions
							content+="<div style=\"display:inline-block; width:200px\">";
							for (const [actionitem, actionitemdata] of Object.entries(contentvalue)) {
								console.log("action ",actionitem,actionitemdata)
								if (actionitem=="passwordresetlink") {
									// {$cb(getsvg("pwresetlink",$svgsize,"#:raw:clear sort#"))}
									content+="<div class=\"clickitem clickitemsvg\" onclick=\"pwresetlink(0,'"+actionitemdata["mib_email"]+"')\">"+'{$cb(getsvg("pwresetlink",$svgsize,"#:raw:send password reset link#"))}</div>';
								}
								if (actionitem=="changeabbreviation") {
									content+="<div class=\"clickitem clickitemsvg\" onclick=\"chgabbrev('"+actionitemdata["mib_email"]+"','"+actionitemdata["currentabbrev"]+"')\">"+'{$cb(getsvg("changeabbrev",$svgsize,"#:raw:modify abbreviation#"))}</div>';
								}
								if (actionitem=="newinvitecode") {
									content+="<div class=\"clickitem clickitemsvg\" onclick=\"chginvite('"+actionitemdata["mib_email"]+"',false)\">"+'{$cb(getsvg("newinvite",$svgsize,"#:raw:generate new invite code#"))}</div>';
								}
								if (actionitem=="removeinvitecode") {
									content+="<div class=\"clickitem clickitemsvg\" onclick=\"chginvite('"+actionitemdata["mib_email"]+"',true)\">"+'{$cb(getsvg("removeinvite",$svgsize,"#:raw:remove invite code#"))}</div>';
								}
							};
							content+="</div>";
						} else {
							value["type"]="text";
						}
					}
					if ((value["type"]=="text") || (value["type"]=="number")) { 
						var cv=contentvalue+""
						cv=cv.toHtmlEntities();
						if (notnull(value["display"])) {
							if (value["display"]=="pretext") {
								var out=cv;
								out=out.replace(/ /g,"&nbsp;");
								out=out.replace(/\\n/g,"<br>");
								content+=out;
							}
							if (value["display"]=="unixtimestamp1000") {
								cv=Math.floor(contentvalue *1000);
								//content+=(contentvalue *1000);
								content+=(new Date(cv).toISOString().slice(0,-1)).replace("T","&nbsp;");
							}
						} else {
							content+=contentvalue; 
						}
					}
					if (value["type"]=="select") { 
						//console.log("TT "+key+" select ",value["select"]);
						selval=contentvalue;
						if (typeof value["select"] === 'object' && !Array.isArray(value["select"])) {
							//console.log("TT "+key+" isobj");
							if (notnull(value["select"][contentvalue])) { 
								selval=value["select"][contentvalue]
							}
						}
						//console.log("TT "+key+" selval",selval);
						content+=selval;
					} 
					content+="</td>";
				}
				alternate=(!alternate);
				newtr=document.createElement("TR");
				if (alternate) {
					newtr.style.backgroundColor="#e0e0e0";
				}
				newtr.innerHTML=content;
				tbody.appendChild(newtr); 
			}
		});
	}
	if (countentries < 1) {
		newtr=document.createElement("TR");
		newtr.innerHTML="<td colspan=4><code>#:raw:no datasets found#</code></td>";
		tbody.appendChild(newtr); 
	}
	var ctlitems=document.getElementsByClassName("scopeselx");
	Array.prototype.forEach.call(ctlitems, function(ctlitem) {
		ctlitem.setAttribute('mdisabled', '1');
	}); 
	sldiv=document.getElementById("scopelocation");
	//sndiv=document.getElementById("scopenavigation");
	//snldiv=document.getElementById("scopenavigation_left");
	//snrdiv=document.getElementById("scopenavigation_right");
	if (jdata.data["dbrequest"]["totalmatches"]<=jdata.data["dbrequest"]["limit"]) {
		sldiv.innerHTML="#:html:datasets# "+(jdata.data["dbrequest"]["offset"]+1)+" - "+jdata.data["dbrequest"]["totalmatches"];
		//snldiv.innerHTML="";
		//snrdiv.innerHTML="";
		totalpages=0; currentpage=0;
	} else {
		Dlimit=jdata.data["dbrequest"]["limit"]
		Doffset=jdata.data["dbrequest"]["offset"]
		lastdsid=jdata.data["dbrequest"]["totalmatches"]-1;
		lastscopedsid=(Doffset+Dlimit-1)
		totalpages=Math.floor(lastdsid / Dlimit)
		currentpage=Math.floor(Doffset / Dlimit)
		navleft=""; navright="";
		navright="<a onclick=\"changescope("+((currentpage+1)*Dlimit)+",null)\">[ &gt; ]</a> &nbsp; <a onclick=\"changescope("+(totalpages*Dlimit)+",null)\">[ &gt;| ]</a>";
		navleft="<a onclick=\"changescope(0,null)\">[ |&lt; ]</a> &nbsp; <a onclick=\"changescope("+((currentpage-1)*Dlimit)+",null)\">[ &lt; ]</a>";
		if (currentpage == totalpages) { 
			lastscopedsid = (jdata.data["dbrequest"]["totalmatches"]-1);
		}
		if (currentpage < totalpages) { 
			
			var ctlitems=document.getElementsByClassName("scopeselxr");
			Array.prototype.forEach.call(ctlitems, function(ctlitem) {
				ctlitem.setAttribute('mdisabled', '0');
			}); 
		} 
		if (currentpage > 0) { 
			var ctlitems=document.getElementsByClassName("scopeselxl");
			Array.prototype.forEach.call(ctlitems, function(ctlitem) {
				ctlitem.setAttribute('mdisabled', '0');
			}); 
		}
		sldiv.innerHTML="#:html:datasets# "+(jdata.data["dbrequest"]["offset"]+1)+" - "+(lastscopedsid+1)+" #:html:of# "+jdata.data["dbrequest"]["totalmatches"];
		//snldiv.innerHTML=navleft;
		//snrdiv.innerHTML=navright;
	}
	console.log("nav currentpage:"+currentpage+" totalpages:"+totalpages)
	
}






function notnull(wvar) {
	return (!((wvar===null) || (typeof wvar== 'undefined')));
}

function refresh() {
	refresh_table_data();
}

function auto_refresh() {
	if (autorefresh===false) {
		autorefreshobj=setInterval(refresh_table_data,30000);
		autorefresh=30;
		refresh();
	} else {
		autorefresh=false;
		clearInterval(autorefreshobj);
	}
	update_indicators();
}

function refresh_table_data() {
	var data={}
	data.tableconfig=tableconfig;
	data.coldata=coldata;
	data.startindex=scopestart;
	data.maxmatches=scopesize;
	data.table="$dbtable";
	rcall("table_getdata",data,refresh_table_data_cb);
}

function refresh_table_data_cb(data) {
	console.log("refresh_table_data_cb: ",data);
	update_table_data(data.jsond);
}

// custom actions (orgunit)
function pwresetlink(progress,mibemail) {
	var rsp;
	rsp=confirm("#:raw:Press OK to send a password reset link to#\\n\\n"+mibemail);
	if (rsp==true) {
		mdata={"mib_email": mibemail}
		rcall("pwresetlink",mdata,pwresetlink_cb)
	}
}
function pwresetlink_cb(rdata) {
	if (rdata.jsond.error===false) {
		alert("#:raw:password reset link sent#");
	} else {
		alert(rdata.jsond.message);
	}
}
function chgabbrev(mibemail,currentabbrev) {
	var rsp;
	if (currentabbrev!="") {
		rsp=confirm("#:raw:CAUTION: this will delete the current email alias# '"+currentabbrev+"'\\n\\n#:raw:Press OK to continue#\\n\\n"+mibemail);
		if (rsp===false) { return false; }
	}
	rsp=prompt("#:raw:Please enter new abbreviation#\\n\\n(#:raw:leave empty for none#)",currentabbrev)
	if (rsp===null) {
		alert("#:raw:no changes performed#");
	}
	mdata={ "mib_email": mibemail,"newabbrev": rsp }
	rcall("chgabbrev",mdata,chgabbrev_cb);
}
function chgabbrev_cb(rdata) {
	if (rdata.jsond.error===false) {
		refresh_table_data();
	}
	alert(rdata.jsond.message);
}
function chginvite(mibemail,removeinvite) {
	var rsp;
	text="#:raw:Press OK to generate a new invite code for#";
	if (removeinvite==true) { text="#:raw:Press OK to remove invite code for#"; }
	rsp=confirm(text+"\\n\\n"+mibemail);
	if (rsp==true) {
		mdata={"mib_email": mibemail, "removeinvite": removeinvite}
		rcall("chginvite",mdata,pwresetlink_cb)
	}
}
function pwresetlink_cb(rdata) {
	if (rdata.jsond.error===false) {
		refresh_table_data();
	}
	alert(rdata.jsond.message);
}



update_table_config();
changescope(0,20);
update_indicators();

</script>
EOM;
return $returnstr;
}


function webif_contents($function) {
	$localdebug=false;
	$replacevars=array();
	$returnstr="";
	$db=db_getlink("self");
	$dbNC=db_getlink("NC");
	if ($function=="settings") {
		$settings_signup_update="";
		if (getvar("subfunction")=="signup") {
			$settings_signup_update="Request results:\n";
			if ($localdebug) {$settings_signup_update="vars:".anytext(array_merge($_GET,$_POST)); }
			if (getvar("update")=="1") {
				if (getvar("settings-signup-initialize")!="") {
					preg_match('/\/([^\/]\w*)\/edit$/m',getvar("settings-signup-initialize"),$match);
					if ($localdebug) { $settings_signup_update.="token match:".anytext($match); }
					if (count($match)!=2) {
						$settings_signup_update.="ERROR: could not initialize; token not found in URL\n";
					} else {
						$token=$match[1];
						$where="where hash='".SQLite3::escapeString($token)."'";
						$nctok=db_get_single_entryC($dbNC,"oc_forms_v2_forms",$where);
						if ($localdebug) { $settings_signup_update.="nctok:".anytext($nctok); }
						if (($nctok===null) or (count($nctok)==0)) {
							$settings_signup_update.="ERROR: could not initialize; token not found in Forms App\n";
						} else {
							$formid=$nctok["id"];
							$acjson=json_encode(array("type" => "public", "users" => array(),"groups" => array()));
							
							$sql="update oc_forms_v2_forms set is_anonymous=0,submit_once=0,expires=0,access_json='".SQLite3::escapeString($acjson)."' where id=".($formid*1).";";
							$dbx=db_exec($dbNC,$sql);
							if (!$dbx) {
								$settings_signup_update.="ERROR: could not update form (".$dbNC->lastErrorMsg().")\n";
							} else {
								$settings_signup_update.="Form ID $formid permissions set for signup form\n";
								$res=db_set_config($db,"form_signup:id",$formid);
								if (!$res) {
									$settings_signup_update.="ERROR: unable to set form as signup form (".$db->lastErrorMsg().")";
								} else {
									$settings_signup_update.="Form with ID $formid set as signup form\n";
									$where="where form_id=".($formid*1)." and \"order\" != 0";
									$ncqn=db_get_multi_entryC($dbNC,"oc_forms_v2_questions",$where);
									if ($localdebug) { $settings_signup_update.="ncqn: $where ".anytext($ncqn); }
									if (!(($ncqn===null) or (count($ncqn)==0))) {
										$settings_signup_update.="Form already contains questions, not auto-provisioning questions\n";
									} else {
										$abbrev=(db_get_config($db,"general:use_abbreviationfield")*1);
										
										$fieldsreq=array(0 => array("desc" => "Invite code","dbfield" => "invitecode","required" => 1),
																		 1 => array("desc" => "Your name","dbfield" => "username","required" => 1),
																		 2 => array("desc" => "Your personal Email address","dbfield" =>"useremail","required" => 1),
																		 3 => array("desc" => "Your phone number","dbfield" => "userphone","required" => 1),
																		 4 => array("desc" => "Abbreviation for ","dbfield" => "abbreviation","required" => 1,"append" => "#:raw:unitdescription# (".$abbrev." #:raw:characters#)"),);
										if ($abbrev < 1) { unset($fieldsreq[4]); }
										$ordercounter=1;
										$maxq=0;
										$sql="select MAX(id) from oc_forms_v2_questions;";
										$dbx=db_query($dbNC,$sql);
										if (isset($dbx[0][0])) { $maxq=$dbx[0][0]*1; }
										foreach($fieldsreq as $fieldid => $fielddata) {
											$req=0; $maxq++;
											if (isset($fielddata["required"]) && ($fielddata["required"]==1)) { $req=1; }
											$desc="#:raw:".$fielddata["desc"]."#";
											if (isset($fielddata["append"])) { $desc.=$fielddata["append"]; }
											$desc=SQLite3::escapeString(replacei8n($desc));
											$sql="insert into oc_forms_v2_questions (id,form_id,'order',type,text,is_required) VALUES (".$maxq.",".($formid*1).",".$ordercounter.",'short','".$desc."',".$req.");";
											$dbx=db_exec($dbNC,$sql);
											$ordercounter++;
											if ($localdebug) { $settings_signup_update.="F ".$fieldid." [$sql] ".$dbNC->lastErrorMsg()." ".anytext($dbx)."\n"; }
											$res=db_set_config($db,"form_signup:field_".$fielddata["dbfield"]."_id",$maxq);
											if ($localdebug) { $settings_signup_update.="  ".$fieldid." [form_signup:field_".$fielddata["dbfield"]."_id] ".$db->lastErrorMsg()." ".anytext($res)."\n"; }
											
										}
										$settings_signup_update.="Form questions auto-provisioned\n";
										
									}
								}
							}
						}
						
					}
				} else {
					if (getvar("settings-signup-formid")!==null) { db_set_config($db,"form_signup:id",getvar("settings-signup-formid")); }
					if (getvar("settings-signup-field_invitecode_id")!==null) { db_set_config($db,"form_signup:field_invitecode_id",getvar("settings-signup-field_invitecode_id")); }
					if (getvar("settings-signup-field_username_id")!==null) { db_set_config($db,"form_signup:field_username_id",getvar("settings-signup-field_username_id")); }
					if (getvar("settings-signup-field_useremail_id")!==null) { db_set_config($db,"form_signup:field_useremail_id",getvar("settings-signup-field_useremail_id")); }
					if (getvar("settings-signup-field_userphone_id")!==null) { db_set_config($db,"form_signup:field_userphone_id",getvar("settings-signup-field_userphone_id")); }
					if (getvar("settings-signup-field_abbreviation_id")!==null) { db_set_config($db,"form_signup:field_abbreviation_id",getvar("settings-signup-field_abbreviation_id")); }
				}
				$settings_signup_update.="Data saved\n";
			}
		}
		$settings_general_update="";
		if (getvar("subfunction")=="general") {
			$settings_general_update="Request results:\n";
			if ($localdebug) { $settings_general_update="vars:".anytext(array_merge($_GET,$_POST)); }
			if (getvar("update")=="1") {
				if (getvar("settings-general-orgunit")!==null) { db_set_config($db,"general:ou_description",getvar("settings-general-orgunit")); }
				if (getvar("settings-general-useabbrev")!==null) { db_set_config($db,"general:use_abbreviationfield",getvar("settings-general-useabbrev")); }
				if (getvar("settings-general-catjson")!==null) { 
					db_set_config($db,"general:categories_items",getvar("settings-general-catjson")); 
					if (!isJson(getvar("settings-general-catjson"))) {
						$settings_general_update.="invalid JSON: categories.\n";
					}
				}
				if (getvar("settings-general-cuijson")!==null) { 
					db_set_config($db,"general:custominfo_items",getvar("settings-general-cuijson")); 
					if (!isJson(getvar("settings-general-cuijson"))) {
						$settings_general_update.="invalid JSON: custominfo.\n";
					}
				}
				$settings_general_update.="Data saved.\n";
			}
		}
		$settings_email_update="";
		if (getvar("subfunction")=="email") {
			$settings_email_update="Request results:\n";
			if ($localdebug) { $settings_email_update="vars:".anytext(array_merge($_GET,$_POST)); }
			if (getvar("update")=="1") {
				if (getvar("settings-email-confirm")!==null) { db_set_config($db,"email:confirm",getvar("settings-email-confirm")); }
				if (getvar("settings-email-confirm_invalidcode")!==null) { db_set_config($db,"email:confirm_invalidcode",getvar("settings-email-confirm_invalidcode")); }
				if (getvar("settings-email-success")!==null) { db_set_config($db,"email:success",getvar("settings-email-success")); }
				if (getvar("settings-email-handover")!==null) { db_set_config($db,"email:handover",getvar("settings-email-handover")); }
				if (getvar("settings-email-pwresetlink")!==null) { db_set_config($db,"email:pwresetlink",getvar("settings-email-pwresetlink")); }
				if (getvar("settings-email-pwreset")!==null) { db_set_config($db,"email:pwreset",getvar("settings-email-pwreset")); }
				if (getvar("settings-email-senderaccount")!==null) { db_set_config($db,"email:senderaccount",getvar("settings-email-senderaccount")); }
				if (getvar("settings-email-sendername")!==null) { db_set_config($db,"email:sendername",getvar("settings-email-sendername")); }
				if (getvar("settings-email-signature")!==null) { db_set_config($db,"email:signature",getvar("settings-email-signature")); }
				$settings_email_update.="Data saved.\n";
			}
		}
		// settings: general
		$replacevars["general:ou_description"]=db_get_config($db,"general:ou_description");
		$replacevars["general:use_abbreviationfield"]=db_get_config($db,"general:use_abbreviationfield");
		$replacevars["general:categories_items"]=db_get_config($db,"general:categories_items");
		$replacevars["general:custominfo_items"]=db_get_config($db,"general:custominfo_items");
		if ($settings_general_update!="") {
			$settings_general_update="<pre>".$settings_general_update."</pre>";
		}
		// settings: signup
		$formid=db_get_config($db,"form_signup:id");
		$replacevars["form_signup:id"]=$formid;
		$replacevars["form_signup:field_invitecode_id"]=db_get_config($db,"form_signup:field_invitecode_id");
		$replacevars["form_signup:field_username_id"]=db_get_config($db,"form_signup:field_username_id");
		$replacevars["form_signup:field_useremail_id"]=db_get_config($db,"form_signup:field_useremail_id");
		$replacevars["form_signup:field_userphone_id"]=db_get_config($db,"form_signup:field_userphone_id");
		$replacevars["form_signup:field_abbreviation_id"]=db_get_config($db,"form_signup:field_abbreviation_id");
		$replacevars["form_signup:field_invitecode_name"]="#:html:- unset -#";
		$replacevars["form_signup:field_username_name"]="#:html:- unset -#";
		$replacevars["form_signup:field_useremail_name"]="#:html:- unset -#";
		$replacevars["form_signup:field_userphone_name"]="#:html:- unset -#";
		$replacevars["form_signup:field_abbreviation_name"]="#:html:- unset -#";
		if (($formid!==null) && ($formid!="")) {
			$replacevars["form_signup:field_invitecode_name"]  =db_get_single_entryC_or($dbNC,"oc_forms_v2_questions","where form_id=".($formid*1)." and \"order\" > 0 and id=".$replacevars["form_signup:field_invitecode_id"],"text","#:html:- unset -#");
			$replacevars["form_signup:field_username_name"]    =db_get_single_entryC_or($dbNC,"oc_forms_v2_questions","where form_id=".($formid*1)." and \"order\" > 0 and id=".$replacevars["form_signup:field_username_id"],"text","#:html:- unset -#");
			$replacevars["form_signup:field_useremail_name"]   =db_get_single_entryC_or($dbNC,"oc_forms_v2_questions","where form_id=".($formid*1)." and \"order\" > 0 and id=".$replacevars["form_signup:field_useremail_id"],"text","#:html:- unset -#");
			$replacevars["form_signup:field_userphone_name"]   =db_get_single_entryC_or($dbNC,"oc_forms_v2_questions","where form_id=".($formid*1)." and \"order\" > 0 and id=".$replacevars["form_signup:field_userphone_id"],"text","#:html:- unset -#");
			$replacevars["form_signup:field_abbreviation_name"]=db_get_single_entryC_or($dbNC,"oc_forms_v2_questions","where form_id=".($formid*1)." and \"order\" > 0 and id=".$replacevars["form_signup:field_abbreviation_id"],"text","#:html:- unset -#");
		}
		if ($settings_signup_update!="") {
			$settings_signup_update="<pre>".$settings_signup_update."</pre>";
		}
		// settings: email
		$replacevars["email:confirm"]=db_get_config($db,"email:confirm");
		$replacevars["email:confirm_invalidcode"]=db_get_config($db,"email:confirm_invalidcode");
		$replacevars["email:success"]=db_get_config($db,"email:success");
		$replacevars["email:handover"]=db_get_config($db,"email:handover");
		$replacevars["email:pwresetlink"]=db_get_config($db,"email:pwresetlink");
		$replacevars["email:pwreset"]=db_get_config($db,"email:pwreset");
		$replacevars["email:senderaccount"]=db_get_config($db,"email:senderaccount");
		$replacevars["email:sendername"]=db_get_config($db,"email:sendername");
		$replacevars["email:signature"]=db_get_config($db,"email:signature");
		if ($settings_email_update!="") {
			$settings_email_update="<pre>".$settings_email_update."</pre>";
		}
		$emailhelp=replacei8n("#:raw:_email_info#");
		$replc=getmailreplacers("*");
		$repstr="";
		foreach($replc as $reptok => $repinfo) {
			$repstr.="<p><code>%".$reptok."%</code> ".replacei8n("#:html:_replacer_".$reptok."#");
		}
		$emailhelp=str_replace("%replacers%",$repstr,$emailhelp);
		$returnstr=<<<EOM
<h2>#:html:Settings#</h2>
<h3>#:html:General#</h3>
$settings_general_update
<form class="form-horizontal" role="form" method="post">
<input type=hidden name=function value=settings>
<input type=hidden name=subfunction value=general>
<input type hidden name=update value=1>
<div class="form-group backup-target-s3">
  <label for="settings-general-orgunit" class="col-sm-2 control-label">#:html:Name of organisational unit#</label>
  <div class="col-sm-8">
      <input type="text" placeholder="Name" class="form-control" rows="1" id="settings-general-orgunit" name="settings-general-orgunit"  #:inputvalue:general:ou_description#>
      <div class="small" style="margin-top: 2px">#:html:_ou_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-general-useabbrev" class="col-sm-2 control-label">#:html:Use abbreviation field#</label>
  <div class="col-sm-8">
      <input type="text" placeholder="" class="form-control" rows="1" id="settings-general-useabbrev" name="settings-general-useabbrev"  #:inputvalue:general:use_abbreviationfield#>
      <div class="small" style="margin-top: 2px">#:html:_general_useabbrev_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-general-catjson" class="col-sm-2 control-label">#:html:JSON for categories#</label>
  <div class="col-sm-8">
      <textarea rows=4 placeholder="" class="form-control" id="settings-general-catjson" name="settings-general-catjson">#:raw:general:categories_items#</textarea>
      <div class="small" style="margin-top: 2px">#:html:_general_categories_items_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-general-cuijson" class="col-sm-2 control-label">#:html:JSON for custom info#</label>
  <div class="col-sm-8">
      <textarea rows=4 placeholder="" class="form-control" id="settings-general-cuijson" name="settings-general-cuijson">#:raw:general:custominfo_items#</textarea>
      <div class="small" style="margin-top: 2px">#:html:_general_custominfo_items_info#</div>
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button id="set-s3-backup-button" type="submit" class="btn btn-primary">#:html:Save#</button>
  </div>
</div>
</form>


<h3>#:html:Sign up form#</h3>
$settings_signup_update
#:raw:_signup_info#
<form class="form-horizontal" role="form" method="post">
<input type=hidden name=function value=settings>
<input type=hidden name=subfunction value=signup>
<input type hidden name=update value=1>

<div class="form-group backup-target-s3">
  <label for="settings-signup-initialize" class="col-sm-2 control-label">#:html:Initialize#</label>
  <div class="col-sm-8">
      <input type="text" placeholder="URL" class="form-control" rows="1" id="settings-signup-initialize" name="settings-signup-initialize">
      <div class="small" style="margin-top: 2px">#:html:_initialize_info#</div>
  </div>
</div>
<div class="form-group backup-target-local backup-target-rsync backup-target-s3 backup-target-b2">
  <label for="settings-signup-formid" class="col-sm-2 control-label">#:html:Form ID#</label>
  <div class="col-sm-8">
      <input type="text" class="form-control" placeholder="ID" rows="1" id="settings-signup-formid" name="settings-signup-formid" #:inputvalue:form_signup:id#>
      <div class="small" style="margin-top: 2px">#:html:_formid_info#</div>
  </div>
</div>
<div class="form-group backup-target-local backup-target-rsync backup-target-s3 backup-target-b2">
  <label for="settings-signup-field_invitecode_id" class="col-sm-2 control-label">#:html:Field ID# '#:html:Invite code#'</label>
  <div class="col-sm-8">
      <input type="text" class="form-control" placeholder="ID" rows="1" id="settings-signup-formid" name="settings-signup-field_invitecode_id" #:inputvalue:form_signup:field_invitecode_id#>
      <div class="small" style="margin-top: 2px">#:html:Current form field name#:<code>#:raw:form_signup:field_invitecode_name#</code><p>#:html:_fieldid_info#</div>
  </div>
</div>
<div class="form-group backup-target-local backup-target-rsync backup-target-s3 backup-target-b2">
  <label for="settings-signup-field_username_id" class="col-sm-2 control-label">#:html:Field ID# '#:html:Your name#'</label>
  <div class="col-sm-8">
      <input type="text" class="form-control" placeholder="ID" rows="1" id="settings-signup-formid" name="settings-signup-field_username_id" #:inputvalue:form_signup:field_username_id#>
      <div class="small" style="margin-top: 2px">#:html:Current form field name#:<code>#:raw:form_signup:field_username_name#</code><p>#:html:_fieldid_info#</div>
  </div>
</div>
<div class="form-group backup-target-local backup-target-rsync backup-target-s3 backup-target-b2">
  <label for="settings-signup-field_useremail_id" class="col-sm-2 control-label">#:html:Field ID# '#:html:Your personal Email address#'</label>
  <div class="col-sm-8">
      <input type="text" class="form-control" placeholder="ID" rows="1" id="settings-signup-formid" name="settings-signup-field_useremail_id" #:inputvalue:form_signup:field_useremail_id#>
      <div class="small" style="margin-top: 2px">#:html:Current form field name#:<code>#:raw:form_signup:field_useremail_name#</code><p>#:html:_fieldid_info#</div>
  </div>
</div>
<div class="form-group backup-target-local backup-target-rsync backup-target-s3 backup-target-b2">
  <label for="settings-signup-field_userphone_id" class="col-sm-2 control-label">#:html:Field ID# '#:html:Your phone number#'</label>
  <div class="col-sm-8">
      <input type="text" class="form-control" placeholder="ID" rows="1" id="settings-signup-formid" name="settings-signup-field_userphone_id" #:inputvalue:form_signup:field_userphone_id#>
      <div class="small" style="margin-top: 2px">#:html:Current form field name#:<code>#:raw:form_signup:field_userphone_name#</code><p>#:html:_fieldid_info#</div>
  </div>
</div>
<div class="form-group backup-target-local backup-target-rsync backup-target-s3 backup-target-b2">
  <label for="settings-signup-field_abbreviation_id" class="col-sm-2 control-label">#:html:Field ID# '#:html:Abbreviation for #'</label>
  <div class="col-sm-8">
      <input type="text" class="form-control" placeholder="ID" rows="1" id="settings-signup-formid" name="settings-signup-field_abbreviation_id" #:inputvalue:form_signup:field_abbreviation_id#>
      <div class="small" style="margin-top: 2px">#:html:Current form field name#:<code>#:raw:form_signup:field_abbreviation_name#</code><p>#:html:_fieldid_info#</div>
  </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button id="set-s3-backup-button" type="submit" class="btn btn-primary">#:html:Save#</button>
  </div>
</div>
</form>

<h3>#:html:Email#</h3>
$settings_email_update
$emailhelp
<form class="form-horizontal" role="form" method="post">
<input type=hidden name=function value=settings>
<input type=hidden name=subfunction value=email>
<input type hidden name=update value=1>
<div class="form-group backup-target-s3">
  <label for="settings-email-senderaccount" class="col-sm-2 control-label">#:html:Email sender#</label>
  <div class="col-sm-8">
      <input type="text" placeholder="username" class="form-control" rows="1" id="settings-email-senderaccount" name="settings-email-senderaccount" #:inputvalue:email:senderaccount#>
      <div class="small" style="margin-top: 2px">#:html:_email-senderaccount_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-email-sendername" class="col-sm-2 control-label">#:html:Email sender name#</label>
  <div class="col-sm-8">
      <input type="text" placeholder="username" class="form-control" rows="1" id="settings-email-sendername" name="settings-email-sendername" #:inputvalue:email:sendername#>
      <div class="small" style="margin-top: 2px">#:html:_email-sendername_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-email-signature" class="col-sm-2 control-label">#:html:Email Signature#</label>
  <div class="col-sm-8">
      <textarea rows=4 placeholder="" class="form-control" rows="1" id="settings-email-signature" name="settings-email-signature">#:raw:email:signature#</textarea>
      <div class="small" style="margin-top: 2px">#:html:_email_signature_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-email-confirm" class="col-sm-2 control-label">#:html:Confirmation Email#</label>
  <div class="col-sm-8">
      <textarea rows=4 placeholder="" class="form-control" rows="1" id="settings-email-confirm" name="settings-email-confirm">#:raw:email:confirm#</textarea>
      <div class="small" style="margin-top: 2px">#:html:_email_confirm_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-email-confirm_invalidcode" class="col-sm-2 control-label">#:html:Invalid invitation code Email#</label>
  <div class="col-sm-8">
      <textarea rows=4 placeholder="" class="form-control" rows="1" id="settings-email-confirm_invalidcode" name="settings-email-confirm_invalidcode">#:raw:email:confirm_invalidcode#</textarea>
      <div class="small" style="margin-top: 2px">#:html:_email_confirm_invalidcode_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-email-success" class="col-sm-2 control-label">#:html:Success Email#</label>
  <div class="col-sm-8">
      <textarea rows=4 placeholder="" class="form-control" rows="1" id="settings-email-success" name="settings-email-success">#:raw:email:success#</textarea>
      <div class="small" style="margin-top: 2px">#:html:_email_success_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-email-handover" class="col-sm-2 control-label">#:html:Handover Email#</label>
  <div class="col-sm-8">
      <textarea rows=4 placeholder="" class="form-control" rows="1" id="settings-email-handover" name="settings-email-handover">#:raw:email:handover#</textarea>
      <div class="small" style="margin-top: 2px">#:html:_email_handover_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-email-pwresetlink" class="col-sm-2 control-label">#:html:Password Reset Link Email#</label>
  <div class="col-sm-8">
      <textarea rows=4 placeholder="" class="form-control" rows="1" id="settings-email-pwresetlink" name="settings-email-pwresetlink">#:raw:email:pwresetlink#</textarea>
      <div class="small" style="margin-top: 2px">#:html:_email_pwresetlink_info#</div>
  </div>
</div>
<div class="form-group backup-target-s3">
  <label for="settings-email-pwreset" class="col-sm-2 control-label">#:html:Password Reset Notification Email#</label>
  <div class="col-sm-8">
      <textarea rows=4 placeholder="" class="form-control" rows="1" id="settings-email-pwreset" name="settings-email-pwreset">#:raw:email:pwreset#</textarea>
      <div class="small" style="margin-top: 2px">#:html:_email_pwreset_info#</div>
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button id="set-s3-backup-button" type="submit" class="btn btn-primary">#:html:Save#</button>
  </div>
</div>
</form>


EOM;
	}
	if ($function=="units") {
		// one time!
	
		/*$catitems=array("Landkreis" => array("desc" => "Landkreis", "type" => "text"),
										"Schulart" => array("desc" => "Schulart", "type" => "select", "select" => array("Gymnasium","Werkrealschule","Grundschule")),
										"Schulträger" => array("desc" => "Schulträger", "type" => "text"));
		db_set_config($db,"general:categories_items",json_encode($catitems));
		$cuiitems=array("Schulgröße" => array("desc" => "Schulgröße", "type" => "text"));
		db_set_config($db,"general:custominfo_items",json_encode($cuiitems)); 
		$cats=array("Landkreis" => "Tübingen","Schulart" => "Gymnasium");
		$cui=array("Schulgröße" => "600");
		
		
		$sql="update sl_orgunit set ou_categories='".SQLite3::escapeString(json_encode($cats))."', ou_custominfo='".SQLite3::escapeString(json_encode($cui))."'";
		db_exec($db,$sql); */
		
		
		$cols1=array("id" => array("desc" => "#:html:ID#", "type" => "number"),
							  "name" => array("desc" => "#:html:Name#", "type" => "text"),
							  "abbreviation" => array("desc" => "#:html:Abbreviation#","type" => "text"),
							  "address_postcode" => array("desc" => "#:html:Postcode#","type" => "text"),
							  "address_city" => array("desc" => "#:html:City#","type" => "text"),
							  "address_street" => array("desc" => "#:html:Street#","type" => "text"),
							  "email" => array("desc" => "#:html:Email#","type" => "text"),
							  "external_id" => array("desc" => "#:html:ID (external)#","type" => "text"),
							  //"invitecode_isset" => array("desc" => "#:html:Invite code#","type" => "select","select" => array("true" => "#:raw:unused#","false" => "#:raw:used#")),
							  "invitecode" => array("desc" => "#:html:Invite code#","type" => "text"),
							  "mib_email" => array("desc" => "#:html:_productname# #:html:Email#","type" => "text"),
							  
							  );
		$cols2=array();
		$catitems=array();
		$catitemsc=db_get_config($db,"general:categories_items");
		if (isJson($catitemsc)) {
			$catitems=json_decode($catitemsc,true);
		} else {
			foreach(explode(",",$catitemsc) as $catitem) {
				$catitems[$catitem]=array("desc" => $catitem, "type" => "text");
			}
		}
		foreach($catitems as $catitem => $cidata) {
			$cols2["categories:".$catitem]=$cidata;
			$cols2["categories:".$catitem]["subdata"]=true;
			$cols2["categories:".$catitem]["subdatadetails"]["master"]="categories";
			$cols2["categories:".$catitem]["subdatadetails"]["field"]=$catitem;
		}
		$catitemsc=db_get_config($db,"general:custominfo_items");
		$catitems=array();
		if (isJson($catitemsc)) {
			$catitems=json_decode($catitemsc,true);
		} else {
			foreach(explode(",",$catitemsc) as $catitem) {
				$catitems[$catitem]=array("desc" => $catitem, "type" => "text");
			}
		}
		foreach($catitems as $catitem => $cidata) {
			$cols2["custominfo:".$catitem]=$cidata;
			$cols2["custominfo:".$catitem]["subdata"]=true;
			$cols2["custominfo:".$catitem]["subdatadetails"]["master"]="custominfo";
			$cols2["custominfo:".$catitem]["subdatadetails"]["field"]=$catitem;

		}
		$cols3=array("actions" => array("desc" => "#:html:actions#", "type" => "static"));
		$cols=array_merge($cols1,$cols2,$cols3);
		$tableconfig=array("id" => array ("sort" => false,"filter" => false, "show" => true),
										 "name" => array ("sort" => false,"filter" => false, "show" => true),
		);
		$returnstr=<<<EOM
<h2>#:html:unitdescription#</h2>
<h3>#:html:List#</h3>
EOM;
		$returnstr.=webif_showtable("orgunit",$cols,$tableconfig);
		
	}
	if ($function=="status") {
		$status_submissions_signup_update="";
		$status_submissions_signup="";
		$formid=db_get_config($db,"form_signup:id");
		$invitecode_id=db_get_config($db,"form_signup:field_invitecode_id");
		debuglog("test; formid".anytext($formid),4);
		if ($formid==null) {
			$status_submissions_signup.="<code>#:raw:Sign up form# #:raw:not yet configured#</code>";
		} else {
			$fslastcheck=db_get_single_entry($db,"storage","internal:form_signup_lastcheck");
			$where="where form_id=".($formid*1)." ORDER BY timestamp DESC";
			$subs=db_get_multi_entryC($dbNC,"oc_forms_v2_submissions",$where);
			$status_submissions_signup.="<table class=\"table\" style=\"width: auto\">\n";
			$status_submissions_signup.="<thead><tr><th>#:html:ID#</th><th>#:html:Date#</th><th>#:html:Invite code#</th><th>".db_get_config($db,"general:ou_description")."</th><th>#:html:Status#</th><th>#:html:Date# #:html:updated#</th></thead>\n";
			$maxcols=5; $trstyle="";
			if (($subs===null) or (count($subs)==0)) {
				$status_submissions_signup.="<tr $trstyle><td $tdstyle colspan=$maxcols><code>#:html:Sign up form# #:raw:has no entries#</td></tr>";
			} else {
				$alternate="";
				foreach($subs as $subr => $subdata) {
					$subid=$subdata["id"];
					$where="where submission_id=".($subid*1);
					$subanswers=db_get_multi_entryC($dbNC,"oc_forms_v2_answers",$where);
					$invitecode="#:raw:- unset -#";
					$updatedts=$subdata["timestamp"];
					$subdetails=array();
					foreach($subanswers as $said => $sadata) {
							$where="where config_key LIKE 'form_signup:field_%' and config_value=".($sadata["question_id"]*1);
							$fieldid=db_get_multi_entryC($db,"sl_config",$where);
							if (!isset($fieldid[0]["config_key"])) {
								//errormessage("! field id ".($sadata["question_id"]*1)." from submission $subid not found");
								//die();
							}
							$detailsname=str_replace("form_signup:field_","",$fieldid[0]["config_key"]);
							//debuglog("  Q $where\n  A[fieldid]".anytext($fieldid),9);
							$subdetails[$detailsname]=$sadata["text"];
					}
					$orgunitdetails="#:html:unknown#";
					if (isset($subdetails["invitecode_id"])) { 
						$invitecode=$subdetails["invitecode_id"];
						$oudata=db_get_single_entry($db,"storage","signup:".$subid.":oudata");
						//$orgunitdetails="<pre>".anytext(unserialize($oudata))."</pre>";
						if ($oudata!==null) { 
							$oudata=unserialize($oudata);
							$where="where ou_id=".($oudata["ou_id"]*1).";";
							$icresult=db_get_single_entryC($db,"sl_orgunit",$where);
							//$orgunitdetails.="<pre>$where\n".anytext($icresult)."</pre>";
							if ($icresult!==null) {
								$orgunitdetails=$icresult["ou_name"]." ".$icresult["ou_address_city"];
							}
						}
					}
					$sstatus=db_get_single_entry($db,"storage","signup:".$subid.":status");
					if ($sstatus===null) { $sstatus="notprocessed"; }
					$supdate=db_get_single_entry($db,"storage","signup:".$subid.":lastupdate");
					if ($supdate!==null) { $updatedts=$supdate; }
					
					// invitecode_format($invitecode)
					$status_submissions_signup.="<tr $trstyle$alternate><td>$subid</td><td>".date("Y-m-d H:i:s",$subdata["timestamp"])."</td><td>".invitecode_format($invitecode)."</td><td>".$orgunitdetails."</td><td>#:html:_substatus_".$sstatus."#</td><td>".date("Y-m-d H:i:s",$updatedts)."</td></tr>\n";
				
					
					
					
					//$status_submissions_signup.="<tr $trstyle $alternate><td>$subid</td><td>".date("Y-m-d H:i:s",$subdata["timestamp"])."</td><td><pre>".anytext($subanswers)."</pre></td></tr>";
					if ($alternate=="") { $alternate="style=\"background-color:#e0e0e0\""; } else { $alternate=""; }
				}
			}
			$status_submissions_signup.="</table>";
		}
		
		
		
		$returnstr=<<<EOM
<h2>#:html:Status#</h2>
<h3>#:html:Form Submissions#: #:html:Sign up form#</h3>
$status_submissions_signup_update
$status_submissions_signup
EOM;
}
	/*if ($function=="logx") {
		$returnstr=<<<EOM
<h2>#:html:Log#</h2>
EOM;
		$returnstr.=db_getlog_html();
	}
	if ($returnstr=="") {
	$returnstr=<<<EOM
	<h2>Schule Control Panel</h2>
	<p>#:html:Select from the navigation bar above.#
EOM;
	} */
	
	if ($function=="log") {
		$returnstr=<<<EOM
<h2>#:html:Log#</h2>
EOM;
		
		
		$cols=array("id" => array("desc" => "#:html:ID#", "type" => "number"),
							  "datetime" => array("desc" => "#:html:Date#","display" => "unixtimestamp1000"),
							  "pid" => array("desc" => "#:html:PID#", "type" => "number"),
							  "severity" => array("desc" => "#:html:Level#", "type" => "number"),
							  "text" => array("desc" => "#:html:Text#","display" => "pretext", "type" => "text"),
							  
							  );
		$tableconfig=array("datetime" => array ("sort" => "DESC","filter" => false, "show" => true),
										 
		);
		$returnstr.=webif_showtable("log",$cols,$tableconfig);
}



	$replacevars["maincontainer"]=$returnstr;
	return $replacevars;
	
}


function webif_base($settings) {
	
	$stylesheet=getstylesheet();
	
	$htmldata=<<<EOM
	<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>#:html:_productname# [#:html:__systemname#] - Schule Control Panel</title>

        <meta name="robots" content="noindex, nofollow">

$stylesheet
    </head>
    <body>

    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="?">#:html:_productname# [#:html:__systemname#]</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!---li class="dropdown if-logged-in-admin">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">System <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#system_status" onclick="return show_panel(this);">Status Checks</a></li>
                <li><a href="#tls" onclick="return show_panel(this);">TLS (SSL) Certificates</a></li>
                <li><a href="#system_backup" onclick="return show_panel(this);">Backup Status</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Advanced Pages</li>
                <li><a href="#custom_dns" onclick="return show_panel(this);">Custom DNS</a></li>
                <li><a href="#external_dns" onclick="return show_panel(this);">External DNS</a></li>
                <li><a href="#munin" onclick="return show_panel(this);">Munin Monitoring</a></li>
              </ul>
            </li --->
            <li><a href="?function=units" class="if-logged-in">#:html:unitdescription#</a></li>
            <li><a href="?function=settings" class="if-logged-in">#:html:Settings#</a></li>
            <li><a href="?function=status" class="if-logged-in">#:html:Status#</a></li>
            <li><a href="?function=log" class="if-logged-in">#:html:Log#</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="if-logged-in"><a href="/cloud" style="color: white">#:html:Back to NextCloud#</a></li>
            <li><div id="loadindicator" class="loadindi"><div></div><div></div><div></div><div></div></div></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="container">#:raw:maincontainer#
    </div>
    <div class="footer">powered by #:html:_productname# #:html:__systemversion# #:html:__licenseinfo# [#:html:__systemname#] #:html:__currentdatetime#<br><a target="_blank" href="https://codeberg.org/tobo/schulbox">#:html:_instructions_support#</a>
    </div>
    <script>loadindicator(false);</script>
</body>
</html>
EOM;
return $htmldata;
}


function geti8nvars($lang) {
	global $selfversion;
  $vars_de=array(
		"Back to NextCloud" => "zurück zu NextCloud",
		"Settings" => "Einstellungen",
		"Select from the navigation bar above." => "Bitte aus der Navigationsleiste oben auswählen.",
		"Sign up form" => "Anmeldeformular",
		"_signup_info" => "<p>Das Anmeldeformular wird genutzt, damit Benutzende ihren Account aktualisieren k&ouml;nnen.\n<p>Um das zu nutzen, lege bitte ein leeres Formular über die Forms-App an und kopiere die URL aus der entsprechenden Zeile des Browsers (sieht ungef&auml;hr so aus: <code>https://boxname.my.domain/cloud/index.php/apps/forms/Ti74TFXD5fE35joR/edit</code>) in das Feld <b>#:html:Initialize#</b> unten.\n<p>Ein Skript wird dann die notwendigen Felder im Formular anlegen und die entsprechenden Details unten ausf&uuml;llen.\n<p>Sobald das erledigt ist, kannst Du die Felder im Formular mithilfe der Forms-App umsortieren oder umbenennen, wie Du magst.",
		"Initialize" => "Einrichtung",
		"Form ID" => "Formular-ID",
		"Save" => "Speichern",
		"Invite code" => "Einladungscode",
		"Your name" => "Dein Name",
		"Your personal Email address" => "Deine persönliche E-Mail-Adresse",
		"Your phone number" => "Deine Mobilnummer",
		"Abbreviation for " => "Kürzel von ",
		"Field ID" => "Feld-ID",
		"- unset -" => "- nicht gesetzt -",
		"Current form field name" => "Aktueller Feldname",
		"General" => "Allgemein",
		"Name of organisational unit" => "Name der Organisationseinheit",
		"Organisational unit" => "Organisationseinheit",
		"Use abbreviation field" => "Kürzelfeld",
		"_ou_info" => "Name, der im Programm für die Organisationseinheit verwendet wird. Zum Beispiel \"SMV\", \"Schule\", ...\nSollte nicht verändert werden, nachdem er einmal gesetzt ist.",
		"_fieldid_info" => "Interne ID des entsprechenden Feldes; wird üblicherweise vom Skript gesetzt und sollte nicht verändert werden.",
		"_formid_info" => "Interne ID des Formulars; wird üblicherweise vom Skript gesetzt und sollte nicht verändert werden.",
		"_initialize_info" => "Einrichtung über die URL des Formulars. Siehe oben.",
		"Email" => "E-Mail",
		"Confirmation Email" => "Bestätigungsmail",
		"_email_info" => "Text f&uuml;r E-Mails, die vom System versendet werden; folgende Platzhalter sind k&ouml;nnen verwendet werden:".
				"%replacers%".
				"<p>Zusätzlich können in den meisten E-Mail auch noch Betreff und/oder Kopienempfänger angepasst werden.".
				"<p>Dazu wird am Anfang der E-Mail-Vorlage ein Abschnitt eingef&uuml;gt, der so aussehen kann:</p>".
				"<p><pre>---subject: Mail von %sender% <br>---cc: admin@meine.domain<br>---bcc: mich@zuhause.domain, stellvertretung@woanders.domain<br>Hallo AnwenderIn<br>...</pre>",
		"_email_confirm_info" => "Diese E-Mail wird versendet, um die im Formular eigegebene E-Mail-Adresse zu bestätigen, wenn der Einladungscode richtig war.",
		"Invalid invitation code Email" => "Falscher Einladungscode E-Mail",
		"_email_confirm_invalidcode_info" => "Diese E-Mail wird versendet, wenn der Einladungscode nicht richtig war.",
		"Success Email" => "Erfolgreiche Registrierung Email",
		"_email_success_info" => "Diese E-Mail wird versendet, wenn die Registrierung erfolgreich abgeschlossen wurde.\nSie sollte Details zu den nächsten Schritten enthalten.",
		"Handover Email" => "Übergabe E-Mail",
		"_email_handover_info" => "Diese E-Mail wird an die bisherige Weiterleitungsadresse versendet, wenn die Registrierung erfolgreich abgeschlossen wurde.",
		"Email Signature" => "E-Mail Signatur",
		"_email_signature_info" => "Dieser Text ist dafür gedacht, über das Feld %signature% an E-Mails angehängt zu werden.",

		"_email:success" => "Erfolgreich",
		"_email:confirm_invalidcode" => "Fehler: Ungültiger Einladungscode",
		"_email:confirm" => "Fast fertig",
		"_email:handover" => "Übergabe des Accounts",
		"Email sender" => "E-Mail-Absender",
		"_email-senderaccount_info" => "Account des Abenders für automatisch versendete E-Mails. z.B. \"admin\" oder \"me\"",
		"Email sender name" => "Name des E-Mail-Absenders",
		"_email-sendername_info" => "Name des Abenders für automatisch versendete E-Mails. z.B. \"Administrator\" oder \"Elternbeirat\" oder \"Landesschülerbeirat\"",
		"_replacer_confirmurl" => "Link, der angeklickt werden soll, um den Empfang der E-Mail zu bestätigen",
		"_replacer_confirmtoken" => "Nur das Token, das zum Bestätigen der E-Mail verwendet wird",
		"_replacer_passwordlink" => "Rücksetzlink für Passwort",
		"_replacer_maillink" => "Link zum Online-Mailer",
		"_replacer_cloudlink" => "Link zur NextCloud",
		"_replacer_mainaddress" => "Haupt-Email-Adresse",
		"_replacer_personaladdress" => "persönliche E-Mail-Adresse",
		"_replacer_personname" => "EmpfängerInnen-Name",
		"_replacer_aliasaddresses" => "zusätzliche Email-Adressen",
		"_replacer_sender" => "Absendername (siehe #:raw:Settings# -> #:raw:Email sender name#)",
		"_replacer_signature" => "Signatur für automatisch versendete E-Mails",
		"Error" => "Fehler",
		"Invalid link" => "Ungültiger Link",
		"Internal Error" => "Interner Fehler",
		"Email address already confirmed" => "E-Mail-Adresse wurde bereits bestätigt",
		"_success_web" => "Juhu, Glückwunsch!",
		"_success_signup_info" => "Du hast Deine E-Mail-Adresse erfolgreich bestätigt.\nWir senden Dir weitere Informationen per E-Mail",
		"Log" => "Log",
		"_productname" => "Schulbox",
		"Status" => "Status",
		"Form Submissions" => "Formulareinträge",

"Date" => "Datum",
"ID" => "ID",
"unknown" => "unbekannt",
"_substatus_confirmed" => "Erfolgreich, E-Mail-Adresse bestätigt",
"_substatus_wait_confirm" => "Warten auf Bestätigung der E-Mail-Adresse",
"_substatus_invalid_code" => "Ungültiger Einladungscode",
"_substatus_no_code" => "Kein Einladungscode angegeben",
"_substatus_notprocessed" => "Formulareintrag noch nicht bearbeitet",
"updated" => "aktualisiert",
"has no entries" => "hat keine Einträge",
"not yet configured" => "noch nicht konfiguriert",
"List" => "Liste",
"Name" => "Name",
"Abbreviation" => "Kürzel",
"Postcode" => "PLZ",
"City" => "Ort",
"Street" => "Straße",
"ID (external)" => "ID (extern)",
"clear sort" => "Sortierung zurücksetzen",
"clear filter" => "Filter zurücksetzen",
"clear all" => "Alles zurücksetzen",
"datasets" => "Datensätze",
"of" => "von",
"per page" => "pro Seite",
"unused" => "ungenutzt",
"used" => "genutzt",

"PID" => "PID",
"refresh" => "aktualisieren",
"auto refresh" => "automatisch",
"Level" => "Detailgrad",
"Text" => "Text",
"remove filter" => "Filter entfernen",
"Value" => "Wert",
"Sort" => "Sortierung",
"ascending" => "aufsteigend",
"descending" => "absteigend",
"Filter" => "Filter",
"condition" => "Bedingung",
"no datasets found" => "keine Datensätze gefunden",
"_directlinkinfo" => "Dieser Link führt direkt zu den Suchergebnissen mit den aktuellen Sortier- und Filtereinstellungen.\nEr kann abgespeichert und zum späteren Zugriff verwendet werden.",
"_instructions_support" => "Support und weitere Informationen",
"next page" => "nächste Seite",
"last page" => "letzte Seite",
"previous page" => "vorhergehende Seite",
"first page" => "erste Seite",
"password reset" => "Passwort zurücksetzen",
"Error" => "Fehler",
"Please enter your schulbox email address" => "Bitte schulbox-E-Mail-Adresse angeben",
"invalid mibemail" => "Unbekannte schulbox-E-Mail-Adresse",
"e.g." => "z.B.",
"continue" => "weiter",
"Please enter your new password" => "Bitte das neue Passwort eingeben",
"please repeat your password" => "Bitte das Passwort wiederholen",

"invalid function call" => "ungültiger Funktionsaufruf",
"unknown function" => "unbekannt Funktion",
"invalid token" => "ungültiges Token",
"valid user" => "Benutzername gültig",
"missing password" => "Passwort fehlt",
"password changed" => "Passwort geändert",


"Password successfully changed" => "Passwort erfolgreich aktualisiert",
"you may now close this window" => "Dieses Fenster kann nun geschlossen werden",
"Bye!" => "Tschüss!",
"passwords do not match" => "Passwörter stimmen nicht überein",
"Link already used" => "Link zum Zurücksetzen des Passworts wurde bereits benutzt",
"send password reset link" => "Link zum zurücksetzen des Passworts an persönliche E-Mail-Adresse schicken",
"actions" => "Aktionen",

"_email:pwresetlink" => "Link zum Zurücksetzen des Passworts",
"_email:pwreset" => "Passwort geändert",
"_email_pwresetlink_info" => "Diese E-Mail wird an die persönliche E-Mail-Adresse versendet. Sie enthält einen Link zur Rücksetzung des Passworts.",
"_email_pwreset_info" =>  "Diese E-Mail wird an die persönliche E-Mail-Adresse versendet, wenn das Passwort des Accounts geändert wurde.",
"Password Reset Notification Email" =>"E-Mail für Passwortänderung",
"Password Reset Link Email" => "E-Mail für Passwortrücksetzungslink",
"_general_custominfo_items_info" =>"JSON-Felderbeschreibung für den Bereich Sonstige Infos (Details siehe oben)",
"_general_categories_items_info" =>"JSON-Felderbeschreibung für den Bereich Kategorien",
"JSON for categories" => "Kategorien-Felder",
"JSON for custom info" => "Sonstige Infos-Felder",
"Press OK to send a password reset link to" => "Nach Drücken auf 'OK' wird ein Passwortrücksetzunglink versendet für",
"password reset link sent" => "Passwortrücksetzungslink wurde versendet",
"_general_useabbrev_info" => "Gib an, Ob das Kürzelfeld genutzt werden soll und wenn ja, welche Maximallänge verwendet werden soll.\nDas Kürzelfeld kann für ein E-Mail-Alias verwendet werden. Wenn der Wert auf '0' gesetzt ist, wird das Kürzelfeld nicht verwendet. Werte größer als 0 geben die Maximallänge des Kürzels an.",
"characters" => "Zeichen",
"__systemversion" => $selfversion,
"__licenseinfo" => "community edition",
"__currentdatetime" => "?",
"__systemname" => "?",
"changed abbreviation to" => "Kürzel geändert auf",
"CAUTION: this will delete the current email alias" => "ACHTUNG: dadurch wird das folgende E-Mail-Alias gelöscht: ",
"Press OK to continue" => "Weiter mit 'OK'",
"Please enter new abbreviation" => "Neues Kürzel:",
"leave empty for none" => "Leerlassen entfernt bisheriges Kürzel",
"no changes performed" => "Keine Änderungen durchgeführt",
"modify abbreviation" => "Kürzel/E-Mail-Alias ändern",
"Press OK to generate a new invite code for" => "Es wird ein neuer Einladungscode erstellt für",
"Press OK to remove invite code for" => "Einladungscode löschen für",
"generate new invite code" => "Neuen Einladungscode erstellen",
"new invite code generated" => "Neuer Einladungscode wurde erstellt",
"remove invite code" => "Einladungcode entfernen",
"invite code removed" => "Einladungscode wurde entfernt",

  );
  $vars_en=array(
		"_signup_info" => "<p>This refers to the public form where users can update their account.\n<p>To use ths feature, go to the Forms app an create an empty form, then copy the URL (looks like <code>https://boxname.my.domain/cloud/index.php/apps/forms/Ti74TFXD5fE35joR/edit</code>) to the field <b>initialize</b> below.\n<p>A script will then automatically generate all required fields within the form and fill the remaining details below.\n<p>Once done, you may reorder and rename the form fields in the Forms App as per your requirements.",
);
  $vars=$vars_de;
  $vars["__currentdatetime"]=date("Y-m-d H:i:s");
  $vars["__systemname"]=getconfig("mailhost");
  if (function_exists("licenseinfo")) {
  	list($vars["__licenseinfo"],$licrs)=licenseinfo(getconfig());
  	//debuglog("licrs:".anytext($licrs),3);
  }
  return $vars;
}

function getstylesheet() {
	$posturl=$_SERVER["PHP_SELF"]."?ajax=1";
	return <<<EOM
        <link rel="stylesheet" href="/admin/assets/bootstrap/css/bootstrap.min.css">
        <style>
	    body {
		overflow-y: scroll;
                padding-bottom: 20px;
            }

            p {
              margin-bottom: 1.25em;
            }

            h1, h2, h3, h4 {
              font-family: sans-serif;
              font-weight: bold;
            }

            h2 {
              margin: 1em 0;
            }

            h3 {
              font-size: 130%;
              border-bottom: 1px solid black;
              padding-bottom: 3px;
              margin-bottom: 13px;
              margin-top: 30px;
            }
              .panel-heading h3 {
                border: none;
                padding: 0;
                margin: 0;
              }

            h4 {
              font-size: 110%;
              margin-bottom: 13px;
              margin-top: 18px;
            }
              h4:first-child {
                margin-top: 6px;
              }

            .admin_panel {
              display: none;
            }

            table.table {
              margin: 1.5em 0;
            }

	    ol li {
	       margin-bottom: 1em;
	    }
          
          .msgbox 
          {
        background: #222222;
        color: white;
        border-radius: 1em;
        padding: 3em;
        position: absolute;
        top: 50%;
        left: 50%;
        min-width:350px;
        margin-right: -50%;
        transform: translate(-50%, -50%) }
          
            .if-logged-in { display: none; }
            .if-logged-in-admin { display: none; }

      /* The below only gets used if it is supported */
      @media (prefers-color-scheme: dark) {
        /* Invert invert lightness but not hue */
        html {
          filter: invert(100%) hue-rotate(180deg);
        }

        /* Set explicit background color (necessary for Firefox) */
        html {
          background-color: #111;
        }
          
        /* Override Boostrap theme here to give more contrast. The black turns to white by the filter. */
        .form-control {
          color: black !important;
        }          

        /* Revert the invert for the navbar */
        button, div.navbar {
          filter: invert(100%) hue-rotate(180deg);
        }
          
        /* Revert the revert for the dropdowns */
        ul.dropdown-menu {
          filter: invert(100%) hue-rotate(180deg);
        }
      }


.footer {
	border-top: 1px solid #444444;
	margin-top: 10px; padding-top:10px;
	color: #444444;
	text-align: center;
	font-size: 9pt;
}


// load indicator
.loadindi {
  display: inline-block;
  position: relative;
  width:30px;
  height: 30px;
}
.loadindi div {
  box-sizing: border-box;
  display: block;
  position: absolute;
  width: 30px;
  height: 30px;
  margin: 8px;
  border:4px solid #fff;
  border-radius: 50%;
  animation: loadindi 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  border-color: #fff transparent transparent transparent;
}
.loadindi div:nth-child(1) {
  animation-delay: -0.45s;
}
.loadindi div:nth-child(2) {
  animation-delay: -0.3s;
}
.loadindi div:nth-child(3) {
  animation-delay: -0.15s;
}
@keyframes loadindi {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}


.thcontrol {
  min-width: 40px; width: 40px; float:left;
}
.thcontainer {
  min-width: 80px;
}
.thbase {
	  
}
.tblnav > li {
	list-style: none;
	//width: 40px;
}
.tblnav > li > a {
	padding: 0px;
	margin:0px;
}
.clickitem {
	min-width: 40px; width: 40px; float:left; text-align:center;
	background-color:lightblue;
	border-radius:4px;
	padding:4px;
	cursor: pointer;
	margin:2px;
}
.clickitem:hover {
	background-color:#ECD0B3;
}
.clickitemsmall {
	min-width: 20px; width: 10px; 
	padding:1px;
	margin:1px;
}
.clickitemlarge {
	min-width: 100px; width: auto; 
}
clickitemsvg {
	min-width: 24px; width: 24px; 
	padding:2px;
	margin:2px;
}
.clickitem[mdisabled="1"] {
	background-color:lightgrey;
	cursor: auto;
}
.clickitem[mdisabled="1"]:hover {
	background-color:lightgrey;
}
.clickitem[mactive="1"] {
	background-color:lightgreen;
	cursor: pointer;
}
.dropdown-menu > .clickitem {
	float: none;
}

.dropdown-menu > .clickitemsmall {
	float: left;
}


        </style>
        <script> 
        function loadindicator(active) {
        	if (active) {
        		document.getElementById("loadindicator").style.display="";
        	} else {
        		document.getElementById("loadindicator").style.display="none";
        	}
        	console.log("loadindicator",active);
        }
        
function rcall(what,mdata,handlefunc) {
	var data={};
	data.what=what;
	data.mdata=mdata;
	loadindicator(true);
	let xhr = new XMLHttpRequest();
	xhr.open("POST","$posturl");
	xhr.setRequestHeader("Content-Type","application/json");
	console.log("rcall request: "+what+" "+handlefunc.name,mdata);
	xhr.send(JSON.stringify(data));
  xhr.onload=function(response) {
  	var jsond=null
  	var returndata={};
  	if (isJsonString(xhr.responseText)) { 
  		returndata.jsond=JSON.parse(xhr.responseText); 
  	}
  	returndata.raw={};
  	returndata.raw.request=data;
  	returndata.raw.event=response;
  	returndata.raw.response=xhr.response;
    console.log("rcall response: "+what+" "+handlefunc.name,returndata);
    loadindicator(false);
    handlefunc(returndata);

};
}
function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
        
        
        </script>
        <link rel="stylesheet" href="/admin/assets/bootstrap/css/bootstrap-theme.min.css">
EOM;
	
}


function svglist() {
	$svglist["directlink"]='<path fill="currentColor" d="M3 1C1.89 1 1 1.89 1 3V14C1 15.11 1.89 16 3 16H14C15.11 16 16 15.11 16 14V11H14V14H3V3H14V5H16V3C16 1.89 15.11 1 14 1M9 7C7.89 7 7 7.89 7 9V12H9V9H20V20H9V18H7V20C7 21.11 7.89 22 9 22H20C21.11 22 22 21.11 22 20V9C22 7.89 21.11 7 20 7H9" />';
	$svglist["refresh_timed"]='<path fill="currentColor" d="M21,10.12H14.22L16.96,7.3C14.23,4.6 9.81,4.5 7.08,7.2C4.35,9.91 4.35,14.28 7.08,17C9.81,19.7 14.23,19.7 16.96,17C18.32,15.65 19,14.08 19,12.1H21C21,14.08 20.12,16.65 18.36,18.39C14.85,21.87 9.15,21.87 5.64,18.39C2.14,14.92 2.11,9.28 5.62,5.81C9.13,2.34 14.76,2.34 18.27,5.81L21,3V10.12M12.5,8V12.25L16,14.33L15.28,15.54L11,13V8H12.5Z" />'; 
	$svglist["refresh"]='<path fill="currentColor" d="M17.65,6.35C16.2,4.9 14.21,4 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20C15.73,20 18.84,17.45 19.73,14H17.65C16.83,16.33 14.61,18 12,18A6,6 0 0,1 6,12A6,6 0 0,1 12,6C13.66,6 15.14,6.69 16.22,7.78L13,11H20V4L17.65,6.35Z" />'; 
	$svglist["filter_on"]='<path fill="currentColor" d="M15,19.88C15.04,20.18 14.94,20.5 14.71,20.71C14.32,21.1 13.69,21.1 13.3,20.71L9.29,16.7C9.06,16.47 8.96,16.16 9,15.87V10.75L4.21,4.62C3.87,4.19 3.95,3.56 4.38,3.22C4.57,3.08 4.78,3 5,3V3H19V3C19.22,3 19.43,3.08 19.62,3.22C20.05,3.56 20.13,4.19 19.79,4.62L15,10.75V19.88M7.04,5L11,10.06V15.58L13,17.58V10.05L16.96,5H7.04Z" />'; 
	$svglist["filter_off"]='<path fill="currentColor" d="M2.39 1.73L1.11 3L9 10.89V15.87C8.96 16.16 9.06 16.47 9.29 16.7L13.3 20.71C13.69 21.1 14.32 21.1 14.71 20.71C14.94 20.5 15.04 20.18 15 19.88V16.89L20.84 22.73L22.11 21.46L15 14.35V14.34L13 12.35L11 10.34L4.15 3.5L2.39 1.73M6.21 3L8.2 5H16.96L13.11 9.91L15 11.8V10.75L19.79 4.62C20.13 4.19 20.05 3.56 19.62 3.22C19.43 3.08 19.22 3 19 3H6.21M11 12.89L13 14.89V17.58L11 15.58V12.89Z" />'; 
	$svglist["sort_on"]='<path fill="currentColor" d="M3,13H15V11H3M3,6V8H21V6M3,18H9V16H3V18Z" />';
	$svglist["sort_off"]='<path fill="currentColor" d="M20.84 22.73L11.11 13H3V11H9.11L6.11 8H3V6H4.11L1.11 3L2.39 1.73L22.11 21.46L20.84 22.73M15 11H14.2L15 11.8V11M21 8V6H9.2L11.2 8H21M3 18H9V16H3V18Z" />';
	$svglist["all_off"]='<path fill="currentColor" d="M17.6 14.2C17.9 13.5 18 12.8 18 12C18 8.7 15.3 6 12 6C11.2 6 10.4 6.2 9.8 6.4L11.4 8H12C14.2 8 16 9.8 16 12C16 12.2 16 12.4 15.9 12.6L17.6 14.2M12 4C16.4 4 20 7.6 20 12C20 13.4 19.6 14.6 19 15.7L20.5 17.2C21.4 15.7 22 13.9 22 12C22 6.5 17.5 2 12 2C10.1 2 8.3 2.5 6.8 3.5L8.3 5C9.4 4.3 10.6 4 12 4M3.3 2.5L2 3.8L4.1 5.9C2.8 7.6 2 9.7 2 12C2 15.7 4 18.9 7 20.6L8 18.9C5.6 17.5 4 14.9 4 12C4 10.2 4.6 8.6 5.5 7.3L7 8.8C6.4 9.7 6 10.8 6 12C6 14.2 7.2 16.1 9 17.2L10 15.5C8.8 14.8 8 13.5 8 12.1C8 11.5 8.2 10.9 8.4 10.3L10 11.9V12.1C10 13.2 10.9 14.1 12 14.1H12.2L19.7 21.6L21 20.3L4.3 3.5L3.3 2.5Z" />';
	$svglist["left"]='<path fill="currentColor" d="M15.41,16.58L10.83,12L15.41,7.41L14,6L8,12L14,18L15.41,16.58Z" />'; 
	$svglist["leftmost"]='<path fill="currentColor" d="M18.41,16.59L13.82,12L18.41,7.41L17,6L11,12L17,18L18.41,16.59M6,6H8V18H6V6Z" />'; 
	$svglist["right"]='<path fill="currentColor" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />'; 
	$svglist["rightmost"]='<path fill="currentColor" d="M5.59,7.41L10.18,12L5.59,16.59L7,18L13,12L7,6L5.59,7.41M16,6H18V18H16V6Z" />'; 
	$svglist["pwresetlink"]='<path fill="currentColor" d="M21.7,13.35L20.7,14.35L18.65,12.3L19.65,11.3C19.86,11.09 20.21,11.09 20.42,11.3L21.7,12.58C21.91,12.79 21.91,13.14 21.7,13.35M12,18.94L18.06,12.88L20.11,14.93L14.06,21H12V18.94M12,14C7.58,14 4,15.79 4,18V20H10V18.11L14,14.11C13.34,14.03 12.67,14 12,14M12,4A4,4 0 0,0 8,8A4,4 0 0,0 12,12A4,4 0 0,0 16,8A4,4 0 0,0 12,4Z" />';
	$svglist["changeabbrev"]='<path fill="currentColor" d="M17,7H22V17H17V19A1,1 0 0,0 18,20H20V22H17.5C16.95,22 16,21.55 16,21C16,21.55 15.05,22 14.5,22H12V20H14A1,1 0 0,0 15,19V5A1,1 0 0,0 14,4H12V2H14.5C15.05,2 16,2.45 16,3C16,2.45 16.95,2 17.5,2H20V4H18A1,1 0 0,0 17,5V7M2,7H13V9H4V15H13V17H2V7M20,15V9H17V15H20Z" />';
	$svglist["newinvite"]='<path fill="currentColor" d="M9,5A4,4 0 0,1 13,9A4,4 0 0,1 9,13A4,4 0 0,1 5,9A4,4 0 0,1 9,5M9,15C11.67,15 17,16.34 17,19V21H1V19C1,16.34 6.33,15 9,15M16.76,5.36C18.78,7.56 18.78,10.61 16.76,12.63L15.08,10.94C15.92,9.76 15.92,8.23 15.08,7.05L16.76,5.36M20.07,2C24,6.05 23.97,12.11 20.07,16L18.44,14.37C21.21,11.19 21.21,6.65 18.44,3.63L20.07,2Z" />';
	$svglist["removeinvite"]='<path fill="currentColor" d="M2,3.27L3.28,2L22,20.72L20.73,22L16.73,18C16.9,18.31 17,18.64 17,19V21H1V19C1,16.34 6.33,15 9,15C10.77,15 13.72,15.59 15.5,16.77L11.12,12.39C10.5,12.78 9.78,13 9,13A4,4 0 0,1 5,9C5,8.22 5.22,7.5 5.61,6.88L2,3.27M9,5A4,4 0 0,1 13,9V9.17L8.83,5H9M16.76,5.36C18.78,7.56 18.78,10.61 16.76,12.63L15.08,10.94C15.92,9.76 15.92,8.23 15.08,7.05L16.76,5.36M20.07,2C24,6.05 23.97,12.11 20.07,16L18.44,14.37C21.21,11.19 21.21,6.65 18.44,3.63L20.07,2Z" />';
	return $svglist;
}

function getsvg($id,$size=24,$title="") {
	$alttext="";
	$svglist=svglist();
	if ($title!="") { $alttext="<title>".$title."</title>"; }

	return "<svg style=\"width:{$size}px;height:{$size}px\" viewBox=\"0 0 24 24\">".$alttext.$svglist[$id]."</svg>";
}
?>
