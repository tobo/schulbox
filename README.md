# schulbox

### intuitive add-on for mail-in-a-box

! CAUTION ! still in early alpha stage, use on your own risk!

## Installation

* install Mail-in-a-box according to instructions provided https://mailinabox.email/
* drop schulbox's provided schulbox.php into /usr/local/lib/owncloud
* change ownership `chown www-data:www-data /usr/local/lib/owncloud/schulbox*.php`
* run init-script `php /usr/local/lib/owncloud/schulbox.php init`
* add regular script-run to crontab `*/2 *    * * *   root    php /usr/local/lib/owncloud/schulbox.php cron`
* enable admin-access for NextCloud according to https://discourse.mailinabox.email/t/admin-login-owncloud/101/2

  tl;dr: `/root/mailinabox/tools/owncloud-unlockadmin.sh me@my.domain`

* log on to Mail-in-a-box's Nextcloud `https://host.my.domain/cloud/`
* install Forms-App in NextCloud and create an empty form, copy form's URL to clipboard
* navigate to `https://host.my.domain/cloud/schulbox.php`

  - Settings / Sign Up Form

    - copy form's URL from clipboard to `Initialize` field and press `save` below

  - Settings / Email

    - fill fields `Email sender` and `Email sender name` and press `save` below



## Usage
* Key location for all _schulbox_ related tasks is `https://host.my.domain/cloud/schulbox.php` (requires NextCloud admin login)
* To allow other users to access _schulbox_ please create a NC group `admin_schulbox` and add users to it